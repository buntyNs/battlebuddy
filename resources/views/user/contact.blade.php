@extends('user.layouts.master')

@section ('content')
<!-- main -->
<section class="breadcrumbs">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li class="active">Contact</li>
        </ol>
    </div>
</section>

<section class="p-b-0">
    <div class="container">
        <div class="heading">
            <i class="fa fa-envelope-open-o"></i>
            <h2>Get in touch with us</h2>
        </div>
    </div>
</section>

<section class="p-t-10">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 mx-auto">
                <form method="post">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" placeholder="Enter your email">
                        <small class="form-text">We'll never share your email with anyone else.</small>
                    </div>
                    <div class="form-group">
                        <label for="reason">Reason</label>
                        <input type="text" class="form-control" id="reason" placeholder="Enter your Reason">
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="subject">Subject</label>
                                <input type="text" class="form-control" id="subject" placeholder="Enter your gamer subject">
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="gamerTag">Gammer Tag</label>
                                <input type="text" class="form-control" id="gamerTag" placeholder="Enter your gamer tag">
                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea id="message" name="message" class="form-control" rows="6"></textarea>
                    </div>
                    <button type="submit" class="btn btn-danger btn-lg btn-rounded btn-effect btn-shadow float-right">
                        Submit
                    </button>
                </form>
            </div>
        </div>
    </div>
</section>



<!-- footer -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-5">
                <h4 class="footer-title">About Battle Buddy</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A adipisci blanditiis deleniti deserunt
                    error, exercitationem incidunt iusto libero modi molestiae repellendus, similique sunt suscipit
                    temporibus,
                    ut vero vitae voluptatem voluptates!
                </p>
                <p>Ab, beatae dolores expedita in minus necessitatibus.
                    Aliquam debitis doloribus, earum esse est ex illum impedit modi nam natus odio placeat quae quos
                    repellat tenetur unde veritatis voluptate voluptates voluptatibus?
                </p>
            </div>
            <div class="col-sm-12 col-md-3">
                <h4 class="footer-title">Platform</h4>
                <div class="row">
                    <div class="col">
                        <ul>
                            <li><a href="#">SUPPORT</a></li>
                            <li><a href="#">JOBS</a></li>
                            <li><a href="#">IMPRESSUM</a></li>
                            <li><a href="/contact">CONTACT US</a></li>
                        </ul>
                    </div>
                    <div class="col">
                        <ul>
                            <li><a href="#">DONATE</a></li>
                            <li><a href="#">PRIVACY POLICY</a></li>
                            <li><a href="#">TERMS OF SERVICE</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <h4 class="footer-title">Subscribe</h4>
                <p>Subscribe to our newsletter and get notification when new games are available.</p>
                <div class="input-group m-t-25">
                    <input type="text" class="form-control" placeholder="Email">
                    <span class="input-group-btn">
            <button class="btn btn-danger" type="button">Subscribe</button>
          </span>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="footer-social">
                <a href="https://facebook.com/yakuthemes" target="_blank" data-toggle="tooltip" title="facebook"><i
                        class="fa fa-facebook"></i></a>
                <a href="https://twitter.com/yakuthemes" target="_blank" data-toggle="tooltip" title="twitter"><i
                        class="fa fa-twitter"></i></a>
                <a href="https://steamcommunity.com/id/yakuzi" target="_blank" data-toggle="tooltip" title="steam"><i
                        class="fa fa-steam"></i></a>
                <a href="https://www.twitch.tv/" target="_blank" data-toggle="tooltip" title="twitch"><i
                        class="fa fa-twitch"></i></a>
                <a href="https://www.youtube.com/user/1YAKUZI" target="_blank" data-toggle="tooltip" title="youtube"><i
                        class="fa fa-youtube"></i></a>
            </div>
            <p>Copyright &copy; 2019 <a
                    href="#" target="_blank">BattleBuddy</a>.
                All rights reserved.</p>
        </div>
    </div>
</footer>
<!-- /footer -->

<!-- /main -->

@endsection
