@extends('user.layouts.master')

@section('content')

  <!-- main -->
  <section class="hero hero-game"  style="background-image: url('https://img.youtube.com/vi/NxI7cPQxAPc/maxresdefault.jpg');">
    <div class="overlay"></div>
    <div class="container">
      <div class="hero-block">
        <div class="hero-left">
          <h2 class="hero-title">Find Your BattleBuddy</h2>
          <p>We All Make Choices But In The End Our Choices Make Us......</p>

        </div>
      </div>
    </div>
  </section>

  <section class="p-t-30">
    <div class="container">
      <div class="row">

        @if (!empty($battleBuddyPC[0]))
        <div class="col-12 col-sm-6 col-md-4">
                <div class="card card-lg">
                  <div class="card-img">
                    <a href="/battle-buddy">
                    <img src="img/game/game-1.jpg" class="card-img-top" alt="{{ $battleBuddyPC[0]->game_name }}">
                  </a>
                    <div class="badge badge-pc">{{ $battleBuddyPC[0]->platform }}</div>
                  </div>
                  <div class="card-block">
                    <h4 class="card-title">
                      <a>{{ $battleBuddyPC[0]->game_name }}</a>
                    </h4>
                    <div class="card-meta">
                      <span>{{ $battleBuddyPC[0]->created_at }}</span>
                    </div>
                    <p class="card-text">{{ $battleBuddyPC[0]->message }}</p>
                  </div>
                </div>
              </div>
        @endif

        @if (!empty($battleBuddyRegion[0]))
              <div class="col-12 col-sm-6 col-md-4">
                <div class="card card-lg">
                  <div class="card-img">
                    <a href="/battle-buddy">
                    <img src="img/game/game-2.jpg" class="card-img-top" alt="{{ $battleBuddyRegion[0]->game_name }}">
                  </a>
                    <div class="badge badge-xbox-one">{{ $battleBuddyRegion[0]->region }}</div>
                  </div>
                  <div class="card-block">
                    <h4 class="card-title">
                      <a>{{ $battleBuddyRegion[0]->game_name }}</a>
                    </h4>
                    <div class="card-meta">
                      <span>{{ $battleBuddyRegion[0]->created_at }}</span>
                    </div>
                    <p class="card-text">{{ $battleBuddyRegion[0]->message }}</p>
                  </div>
                </div>
              </div>
        @endif

        @if (!empty($battleBuddyGames[0]))
              <div class="col-12 col-sm-6 col-md-4">
                <div class="card card-lg">
                  <div class="card-img">
                    <a href="/battle-buddy">
                    <img src="img/game/game-3.jpg" class="card-img-top" alt="{{ $battleBuddyGames[0]->game_name }}">
                  </a>
                    <div class="badge badge-ps4">{{ $battleBuddyGames[0]->language }}</div>
                  </div>
                  <div class="card-block">
                    <h4 class="card-title">
                      <a>{{ $battleBuddyGames[0]->game_name }}</a>
                    </h4>
                    <div class="card-meta">
                      <span>{{ $battleBuddyGames[0]->created_at }}</span>
                    </div>
                    <p class="card-text">{{ $battleBuddyGames[0]->message }}</p>
                  </div>
                </div>
              </div>
            </div>
        @endif
    </div>
  </section>
  <!-- /main -->

<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <!-- post -->
        @if (!empty($latestFeeds))
            @foreach ($latestFeeds as $latestFeed)
            <div class="post">
                <div class="post-header">
                    <div>
                        <a href="/feeds/user/{{ $latestFeed->user_id }}">
                            <img @if ($latestFeed->avatar == 1) src="/img/user/avatar.png" @else src="/storage/{{ $latestFeed->avatar }}"@endif>
                        </a>
                    </div>
                    <div>
                        <h2 class="post-title">
                            <h2 class="post-title"><a href="/feeds/show/{{ $latestFeed->id }}">{{ $latestFeed->name }}</a></h2>
                        </h2>
                        <div class="post-meta">
                            <span><i class="fa fa-clock-o"></i> {{ $latestFeed->created_at }} by <a href="/feeds/user/{{ $latestFeed->user_id }}">{{ ucwords($latestFeed->username) }}</a></span>
                            @if ($latestFeed->like_count != null || $latestFeed->like_count != 0) <span><a><i class="fa fa-heart"></i> {{ $latestFeed->like_count }} likes</a></span> @else <span><a><i class="fa fa-heart-o"></i>0 likes</a></span>@endif
                        </div>
                    </div>
                </div>
                <div class="post-thumbnail">
                    @if (!is_null($latestFeed->image_path))
                    <img src="/storage/{{ $latestFeed->image_path }}" alt="/storage/{{ $latestFeed->name }}">
                    @endif
                    @if (!is_null($latestFeed->video_url))
                    <iframe width="853" height="480" src="{{ $latestFeed->video_url }}" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    @endif
                </div>
                <p>{{ $latestFeed->description }}</p>
                </div>
            @endforeach
        @endif
      </div>

      <!-- sidebar -->
      <div class="col-lg-4">
        <div class="sidebar">
          <!-- widget post -->
          <div class="widget widget-post">
            <h5 class="widget-title">Popular on Gameforest</h5>
            @if (!empty($popularFeeds))
                @foreach ($popularFeeds as $popularFeed)
                    @if (!is_null($popularFeed->image_path))
                    <a href="/feeds/show/{{ $popularFeed->id }}">
                    <img src="/storage/{{ $popularFeed->image_path }}" alt="/storage/{{ $popularFeed->name }}">
                    </a>
                    @endif
                    @if (!is_null($popularFeed->video_url))
                    <a href="/feeds/show/{{ $popularFeed->id }}">
                        <h6 class="post-title">
                            <h5 class="post-title"><a href="/feeds/show/{{ $popularFeed->id }}">{{ $popularFeed->name }}</a></h5>
                        </h6>
                        <iframe width="770" height="200" src="{{ $popularFeed->video_url }}" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"></iframe>
                    </a>
                    @endif
                @endforeach
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
</section>



  <!-- footer -->
  <footer id="footer">
      <div class="container">
          <div class="row">
              <div class="col-sm-12 col-md-5">
                  <h4 class="footer-title">About Battle Buddy</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A adipisci blanditiis deleniti deserunt
                      error, exercitationem incidunt iusto libero modi molestiae repellendus, similique sunt suscipit
                      temporibus,
                      ut vero vitae voluptatem voluptates!
                  </p>
                  <p>Ab, beatae dolores expedita in minus necessitatibus.
                      Aliquam debitis doloribus, earum esse est ex illum impedit modi nam natus odio placeat quae quos
                      repellat tenetur unde veritatis voluptate voluptates voluptatibus?
                  </p>
              </div>
              <div class="col-sm-12 col-md-3">
                  <h4 class="footer-title">Platform</h4>
                  <div class="row">
                      <div class="col">
                          <ul>
                              <li><a href="#">SUPPORT</a></li>
                              <li><a href="#">JOBS</a></li>
                              <li><a href="#">IMPRESSUM</a></li>
                              <li><a href="/contact">CONTACT US</a></li>
                          </ul>
                      </div>
                      <div class="col">
                          <ul>
                              <li><a href="#">DONATE</a></li>
                              <li><a href="#">PRIVACY POLICY</a></li>
                              <li><a href="#">TERMS OF SERVICE</a></li>
                          </ul>
                      </div>
                  </div>
              </div>
              <div class="col-sm-12 col-md-4">
                  <h4 class="footer-title">Subscribe</h4>
                  <p>Subscribe to our newsletter and get notification when new games are available.</p>
                  <div class="input-group m-t-25">
                      <input type="text" class="form-control" placeholder="Email">
                      <span class="input-group-btn">
            <button class="btn btn-danger" type="button">Subscribe</button>
          </span>
                  </div>
              </div>
          </div>
          <div class="footer-bottom">
              <div class="footer-social">
                  <a href="https://facebook.com/yakuthemes" target="_blank" data-toggle="tooltip" title="facebook"><i
                          class="fa fa-facebook"></i></a>
                  <a href="https://twitter.com/yakuthemes" target="_blank" data-toggle="tooltip" title="twitter"><i
                          class="fa fa-twitter"></i></a>
                  <a href="https://steamcommunity.com/id/yakuzi" target="_blank" data-toggle="tooltip" title="steam"><i
                          class="fa fa-steam"></i></a>
                  <a href="https://www.twitch.tv/" target="_blank" data-toggle="tooltip" title="twitch"><i
                          class="fa fa-twitch"></i></a>
                  <a href="https://www.youtube.com/user/1YAKUZI" target="_blank" data-toggle="tooltip" title="youtube"><i
                          class="fa fa-youtube"></i></a>
              </div>
              <p>Copyright &copy; 2019 <a
                      href="#" target="_blank">BattleBuddy</a>.
                  All rights reserved.</p>
          </div>
      </div>
  </footer>
  <!-- /footer -->

@endsection
