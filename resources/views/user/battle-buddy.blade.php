@extends('user.layouts.master')

@section('page-css')
<!-- plugins css -->
{{-- <link rel="stylesheet" href="plugins/switchery/switchery.min.css"> --}}
{{-- <link rel="stylesheet" href="plugins/flatpickr/flatpickr.min.css"> --}}
<link rel="stylesheet" href="/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
crossorigin="anonymous">

@endsection

@section ('content')
<!-- main -->
<section class="hero" style="background-image: url('/img/hero/battle-1.jpg');">
    <div class="overlay"></div>
    <div class="container">
    <div class="hero-block">
{{--        <h2 class="hero-title">Find Your Battle Buddy</h2>--}}

    </div>
    </div>
</section>

<section class="toolbar toolbar-links" data-fixed="true">
    <div class="container">
        <div class="row">
            <div class="toolbar-custom" style="margin-bottom: 5px;">
                <div class="dropdown float-left m-r-10 m-l-10">
                    <select id="platform" class="btn form-control" name="platform">
                        <option value="">Any Platform</option>
                        <option value="playstation">Playstation</option>
                        <option value="xbox_one">Xbox One</option>
                        <option value="pc">PC</option>
                    </select>
                </div>
                <div class="dropdown float-left m-r-10 m-l-10">
                    <select id="region" class="btn form-control" name="region">
                        <option value="">Any Region</option>
                        <option value="europe">Europe</option>
                        <option value="asia">Asia</option>
                        <option value="north_america">North America</option>
                        <option value="south_america">South America</option>
                        <option value="africa">Africa</option>
                        <option value="australia">Australia</option>
                    </select>
                </div>
                <div class="dropdown float-left m-r-10 m-l-10">
                    <select id="language" class="btn form-control" name="language">
                        <option value="">Any Language</option>
                        <option value="english">English</option>
                        <option value="french">French</option>
                        <option value="hindi">Hindi</option>
                    </select>
                </div>
                <div class="dropdown float-left m-r-10 m-l-10">
                    <select id="voice_chat" class="btn form-control" name="voice_chat">
                        <option value="">All</option>
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                </div>
                <div class="dropdown float-left m-r-10 m-l-10">
                    <select id="game_name" class="btn form-control" name="game_name">
                        <option value="">Any Game</option>
                        <option value="call_of_duty">Call of duty</option>
                        <option value="clash_of_the_clans">Clash of the clans</option>
                        <option value="modern_warfare">Modern warfare</option>
                        <option value="far_cry">Far cry</option>
                    </select>
                </div>
            </div>
            <div class="toolbar-custom">
                <div class="dropdown float-left m-r-10 m-l-35">
                    <button id="send_search" class="btn btn-warning" type="button" aria-haspopup="true" onclick="battleBuddySearch()" aria-expanded="true" style="width: 300px; align-self: center">Search</button>
                </div>
            </div>
        </div>
    </div>
</section>

<section  class="p-t-40 p-b-60" >
    <div class="container">
        <div class="row">
            <div id="replace" class="col-lg-8"></div>
            <div id="post-container" class="col-lg-8">

{{--            @if (isset($latestPosts))--}}
{{--                @foreach ($latestPosts as $latestPost)--}}
{{--                <div class="forum-post">--}}
{{--                    <div class="forum-header">--}}
{{--                    <div><a href="#"><img src="/img/user/user-3.jpg"></a></div>--}}
{{--                    <div>--}}
{{--                        <h2 class="forum-title"><a href="/user/profile/my-feeds">{{ $latestPost->username }}</a></h2>--}}
{{--                    </div>--}}
{{--                    <div>--}}
{{--                        #1--}}
{{--                    <span>{{ $latestPost->created_at->diffForHumans() }}</span>--}}
{{--                    </div>--}}
{{--                    </div>--}}
{{--                    <div class="forum-body">--}}
{{--                    <h5 class="m-b-20">{{ $latestPost->platform }}&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;{{ $latestPost->language }}&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;{{ $latestPost->region }}&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;VoiceChat : @if ($latestPost->voice_chat == 1) Yes @else No @endif</h5>--}}

{{--                    <p>{{ $latestPost->message }}</p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                @endforeach--}}
{{--            @endif--}}

            </div>

            <!-- sidebar -->
            <div class="col-lg-4">
                <div class="sidebar">
                    @if (Auth::check())
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title"><i class="fa fa-address-book"></i> Make A Post </h4>
                            </div>
                            <div class="card-block">
                                <form action="/battle-buddy" method="POST" id="battle-buddy">
                                    @csrf
                                <div class="form-group input-icon-left m-b-10">
                                    <i class="fa fa-gamepad"></i>
                                    <select id="game_name" class="form-control form-control-secondary" name="game_name">
                                        {{-- @foreach ($games as $game)
                                        <option value="{{ $game->id }}">{{ $game->name }}</option>
                                        @endforeach --}}
                                        <option value="Call of duty">Call of duty</option>
                                        <option value="Witch">Witch</option>
                                        <option value="IGI2">IGI2</option>
                                        <option value="IGI">IGI</option>
                                        <option value="Clash of clans">Clash of clans</option>
                                        <option value="Far cry">Far cry</option>
                                        <option value="Modern warfare">Modern warfare</option>
                                    </select>
                                    <span class="text-danger">{{ $errors->first('game_name') }}</span>
                                </div>

                                <div class="form-group input-icon-left m-b-10">
                                    <i class="fa fa-laptop"></i>
                                    <select id="platform" class="form-control form-control-secondary" name="platform">
                                        <option value="pc">PC</option>
                                        <option value="playstation">Playstation</option>
                                        <option value="xbox">Xbox</option>
                                    </select>
                                    <span class="text-danger">{{ $errors->first('platform') }}</span>
                                </div>

                                <div class="form-group input-icon-left m-b-10">
                                    <i class="fa fa-globe"></i>
                                    <select id="region" class="form-control form-control-secondary" name="region">
                                        <option value="europe">Europe</option>
                                        <option value="asia">Asia</option>
                                        <option value="north_america">North America</option>
                                        <option value="south_america">South America</option>
                                        <option value="africa">Africa</option>
                                        <option value="australia">Australia</option>
                                    </select>
                                    <span class="text-danger">{{ $errors->first('region') }}</span>
                                </div>

                                <div class="form-group input-icon-left m-b-10">
                                    <i class="fa fa-language"></i>
                                    <select id="language" class="form-control form-control-secondary" name="language">
                                        <option value="english">English</option>
                                        <span class="text-danger">{{ $errors->first('language') }}</span>
                                    </select>
                                </div>

                                <div class="form-group input-icon-left m-b-10">
                                    <i class="fa fa-microphone"></i>
                                    <select name="voice_chat" id="voice_chat" class="form-control form-control-secondary">
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                    <span class="text-danger">{{ $errors->first('voice_chat') }}</span>
                                </div>

                                <div class="form-group input-icon-left m-b-10">
                                    <i class="fa fa-user"></i>
                                    <input type="text" id="gamer_id" class="form-control form-control-secondary" name="gamer_id" placeholder="Gamer ID">
                                    <span class="text-danger">{{ $errors->first('gamer_id') }}</span>
                                </div>

                                <div class="form-group input-icon-left m-b-10">
                                    <i class="fa fa-file"></i>
                                    <textarea id="message" class="form-control form-control-secondary" name="message" rows="4" placeholder="Your message"></textarea>
                                    <span class="text-danger">{{ $errors->first('message') }}</span>
                                </div>

                                <button id="send_form" class="btn btn-danger btn-block" type="submit" role="button">POST</button>
                                <div class="alert alert-success d-none" id="msg_div">
                                    <span id="res_message"></span>
                                </div>
                                </form>
                            </div>
                    </div>
                    @else
                    <a href="/battle-buddy/create" class="btn btn-danger btn-block">Create New Post</a>
                    @endif
                    <br>
                    <!-- widget post  -->
                    <div class="widget widget-post">
                        <h5 class="widget-title">Recommended Feeds</h5>
                        @if (!empty($popularFeeds))
                            @foreach ($popularFeeds as $popularFeed)
                                @if (!is_null($popularFeed->image_path))
                                <a href="/feeds/show/{{ $popularFeed->id }}">
                                    <img src="/storage/{{ $popularFeed->image_path }}" alt="/storage/{{ $popularFeed->name }}">
                                </a>
                                @endif
                                @if (!is_null($popularFeed->video_url))
                                <a href="/feeds/show/{{ $popularFeed->id }}">
                                    <h6 class="post-title">
                                        <h5 class="post-title"><a href="/feeds/show/{{ $popularFeed->id }}">{{ $popularFeed->name }}</a></h5>
                                    </h6>
                                    <iframe width="770" height="200" src="{{ $popularFeed->video_url }}" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"></iframe>
                                </a>
                                @endif
                            @endforeach
                        @endif
                    </div>
{{--                    <div id="chatPanel">--}}
{{--                        <p>Chat Pannel</p>--}}
{{--                    </div>--}}
                    {{-- chat pop-up message start --}}
                    <div class="social-panel-container">
                        <div class="social-panel">
                          <p>ALL CHAT</p>
                          <button class="close-btn"><i class="fas fa-times"></i></button>
                          <h4></h4>
                          <div class="col-md-12">
                            <div class="chat-users ">


                                <div class="users-list" id ="chatPanel">


                                </div>

                            </div>
                        </div>
                        </div>
                      </div>
                      <button class="floating-btn">
                        Messages
{{--                        <span class="badge badge-danger float-right ml-2">5</span>--}}
                      </button>

                       {{-- chat pop-up message end --}}
                </div>

            </div>
        </div>
    </div>
</section>


<!--   chat component start   -->

<div class="chatbox chatbox--tray chatbox--empty">
    <div class="chatbox__title">
        <h5><a href="#" id="chatbox__title" name="none">Chat</a></h5>
        <button class="chatbox__title__tray">
            <span></span>
        </button>
        <!-- <button class="chatbox__title__close">
            <span>
                <svg viewBox="0 0 12 12" width="12px" height="12px">
                    <line stroke="#FFFFFF" x1="11.75" y1="0.25" x2="0.25" y2="11.75"></line>
                    <line stroke="#FFFFFF" x1="11.75" y1="11.75" x2="0.25" y2="0.25"></line>
                </svg>
            </span>
        </button> -->
    </div>
    <div class="chatbox__body" id="chatbox__body">
        <!-- <div class="chatbox__body__message chatbox__body__message--left">
            <img src="https://s3.amazonaws.com/uifaces/faces/twitter/brad_frost/128.jpg" alt="Picture">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </div>
        <div class="chatbox__body__message chatbox__body__message--right">
            <img src="https://s3.amazonaws.com/uifaces/faces/twitter/arashmil/128.jpg" alt="Picture">
            <p>Nulla vel turpis vulputate, tincidunt lectus sed, porta arcu.</p>
        </div>
        <div class="chatbox__body__message chatbox__body__message--left">
            <img src="https://s3.amazonaws.com/uifaces/faces/twitter/brad_frost/128.jpg" alt="Picture">
            <p>Curabitur consequat nisl suscipit odio porta, ornare blandit ante maximus.</p>
        </div>
        <div class="chatbox__body__message chatbox__body__message--right">
            <img src="https://s3.amazonaws.com/uifaces/faces/twitter/arashmil/128.jpg" alt="Picture">
            <p>Cras dui massa, placerat vel sapien sed, fringilla molestie justo.</p>
        </div>
        <div class="chatbox__body__message chatbox__body__message--right">
            <img src="https://s3.amazonaws.com/uifaces/faces/twitter/arashmil/128.jpg" alt="Picture">
            <p>Praesent a gravida urna. Mauris eleifend, tellus ac fringilla imperdiet, odio dolor sodales libero, vel mattis elit mauris id erat. Phasellus leo nisi, convallis in euismod at, consectetur commodo urna.</p>
        </div> -->
    </div>
    <textarea class="chatbox__message" placeholder="Write something interesting"></textarea>
</div>

<!-- chat component end  -->

<!-- /main -->
@endsection

@section('page-scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://js.pusher.com/4.4/pusher.min.js"></script>
<script src="/js/jquery.timeago.js" type="text/javascript"></script>
<!-- jquery validation cdn -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js" integrity="sha256-xLhce0FUawd11QSwrvXSwST0oHhOolNoH9cUXAcsIAg=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js" integrity="sha256-vb+6VObiUIaoRuSusdLRWtXs/ewuz62LgVXg2f1ZXGo=" crossorigin="anonymous"></script>

<!-- plugins js -->
{{-- <script src="/plugins/switchery/switchery.min.js"></script> --}}
{{-- <script src="/plugins/flatpickr/flatpickr.min.js"></script> --}}
{{-- <script src="/plugins/select2/js/select2.min.js"></script> --}}
{{-- custom script for select2 --}}
{{-- <script type="text/javascript">
    (function($) {
    "use strict";
    // select2
    $('.select2').select2({
        placeholder: "Select platform"
    });

    // flatpickr
    $("#date").flatpickr();
    $("#datetime").flatpickr({
        enableTime: true
    });
    $("#dateinline").flatpickr({
        inline: true
    });
    $("#dateclock").flatpickr({
        enableTime: true,
        noCalendar: true,

        enableSeconds: false, // disabled by default

        time_24hr: false, // AM/PM time picker is used by default

        // default format
        dateFormat: "H:i",

        // initial values for time. don't use these to preload a date
        defaultHour: 12,
        defaultMinute: 0
    });

    // switcher
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

    elems.forEach(function(html) {
        var switchery = new Switchery(html);
    });
    })(jQuery);
</script> --}}

{{-- custom script for making a post --}}
<script>

    if ($("#battle-buddy").length > 0) {

        $("#battle-buddy").validate({
        rules: {
            game_name: {
                required: true,
            },
            platform: {
                required: true,
            },
            region: {
                required: true,
            },
            language: {
                required: true,
            },
            voice_chat: {
                required: true,
            },
            gamer_id: {
                required: true,
                maxlength: 30,
                minlength: 3,
            },
            message: {
                required: true,
                maxlength: 100,
                minlength: 3,
            },
            // mobile_number: {
            //     required: true,
            //     digits:true,
            //     minlength: 10,
            //     maxlength:12,
            // },
            // email: {
            //     required: true,
            //     maxlength: 50,
            //     email: true,
            // },
        },
        messages: {
            game_name: {
                required: "Please select a game",
            },
            platform: {
                required: "Please select a platform",
            },
            region: {
                required: "Please select a region",
            },
            language: {
                required: "Please select a language",
            },
            voice_chat: {
                required: "Please select yes or no",
            },
            gamer_id: {
                required: "Please enter a valid gamer id",
                minlength: "Should contain minimum 3 characters",
                maxlength: "Should contain maximum 30 characters",
            },
            message: {
                required: "Please enter a message",
                minlength: "Should contain minimum 3 characters",
                maxlength: "Should contain maximum 100 characters",
            },
            // mobile_number: {
            //     required: "Please enter contact number",
            //     minlength: "The contact number should be 10 digits",
            //     digits: "Please enter only numbers",
            //     maxlength: "The contact number should be 12 digits",
            // },
            // email: {
            //     required: "Please enter valid email",
            //     email: "Please enter valid email",
            //     maxlength: "The email name should less than or equal to 50 characters",
            // },
        },

        submitHandler: function(form) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#send_form').html('Posting...');

            $.ajax({
                url: '{{ env('APP_URL') }}'+'/battle-buddy',
                method: "POST",
                data: $('#battle-buddy').serialize(),
                    success: function( response ) {

                        $('#send_form').html('POST');
                        $('#res_message').show();
                        $('#res_message').html(response.msg);
                        $('#msg_div').removeClass('d-none');

                        var voiceChat = response.data.voice_chat ? "Yes" : "No";
                        $("#post-container").prepend("<div class='forum-post'><div class='forum-header'><div><a href='#'><img src='/img/user/user-3.jpg'></a></div><div><h2 class='forum-title'><a href='#'>" + response.data.username + "</a></h2></div><div>#1<span>" + response.date + "</span></div></div><div class='forum-body'><h5 class='m-b-20'>" + response.data.platform + "&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;" + response.data.language + "&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;" + response.data.region + "&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;VoiceChat : " + voiceChat + "</h5><p>" + response.data.message + "</p></div></div>");

                        document.getElementById("battle-buddy").reset();
                        setTimeout(function() {
                            $('#res_message').hide();
                            $('#msg_div').hide();
                        }, 10000);

                    }
                });



        }
        })
    }


    function battleBuddySearch() {
        var data = {
            "platform":$("#platform").val(),
            "region":$("#region").val(),
            "language":$("#language").val(),
            "voice_chat":$("#voice_chat").val(),
            "game_name":$("#game_name").val()
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#send_search').html('Searching...');

        $.ajax({
            url: '{{ env('APP_URL') }}'+'/battle-buddy/search',
            method: "POST",
            data: data,
                success: function( response ) {
                $('#send_search').html('Search');
                var array = response.data;
                var htmlStr = "";
                if(array.length > 0) {
                    for (let i = 0; i < array.length; i++) {
                        var voiceChat = array[i].voice_chat ? "Yes" : "No";
                        htmlStr += "<div></div><div class='forum-post'><div class='forum-header'><div><a href='#'><img src='/img/user/user-3.jpg'></a></div><div><h2 class='forum-title'><a href='#'>" + array[i].username + "</a></h2></div><div>#1<span>" + "test" + "</span></div></div><div class='forum-body'><h5 class='m-b-20'>" + array[i].platform + "&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;" + array[i].language + "&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;" + array[i].region + "&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;VoiceChat : " + voiceChat + "</h5><p>" + array[i].message + "</p></div></div></div>";
                    }
                } else {
                    htmlStr = "<div>No LFG Entries for the given filter, make one!</div>"
                }
                $("#post-container").remove();
                document.getElementById('replace').innerHTML = htmlStr;
            }
        });
    }


    function chatEnable (id , name) {
        console.log( id , name);

        console.log( $('body').attr('user-id'));
        if( $('body').attr('user-id') === ''){
            alert('Please log in First');
            return;
        }

        var $chatbox = $('.chatbox'),
            $chatboxTitle = $('.chatbox__title'),
            $chatboxTitleClose = $('.chatbox__title__close'),
            $chatboxCredentials = $('.chatbox__credentials');
            $chatbox.toggleClass('chatbox--tray');
            $('#chatbox__title').html(name);
            $('#chatbox__title').attr('name', id);

            $('.chat_count').html('0');

            var data = JSON.parse(window.localStorage.getItem(id));
            console.log();
            if(data !== null){
                $( ".chatbox__body" ).empty();
                for (var key in data) {
                    console.log(data[key]);
                    if(data[key].type === "receve"){
                        $( ".chatbox__body" ).append( '<div class="chatbox__body__message chatbox__body__message--left"><img src="https://s3.amazonaws.com/uifaces/faces/twitter/brad_frost/128.jpg" alt="Picture"><p>'+data[key].message+'</p></div>' );
                    }else{
                        $( ".chatbox__body" ).append( '<div class="chatbox__body__message chatbox__body__message--right"><img src="https://s3.amazonaws.com/uifaces/faces/twitter/arashmil/128.jpg" alt="Picture"><p>'+data[key].message+'</p></div>' );

                    }

                }

            }


    }

    function chatBoxRebuild (id , name) {
        console.log( id , name);
        // var $chatbox = $('.chatbox'),
            // $chatboxTitle = $('.chatbox__title'),
            // $chatboxTitleClose = $('.chatbox__title__close'),
            // $chatboxCredentials = $('.chatbox__credentials');
            // $chatbox.toggleClass('chatbox--tray');
            // $('#chatbox__title').html(name);
            $('#chatbox__title').attr('name');
            console.log();

            if($('#chatbox__title').attr('name') === name){
                $('.chat_count').html('0');

                var data = JSON.parse(window.localStorage.getItem(name));

                if(data !== null){
                    $( ".chatbox__body" ).empty();
                    for (var key in data) {
                        console.log(data[key]);
                        if(data[key].type === "receve"){
                            $( ".chatbox__body" ).append( '<div class="chatbox__body__message chatbox__body__message--left"><img src="https://s3.amazonaws.com/uifaces/faces/twitter/brad_frost/128.jpg" alt="Picture"><p>'+data[key].message+'</p></div>' );
                        }else{
                            $( ".chatbox__body" ).append( '<div class="chatbox__body__message chatbox__body__message--right"><img src="https://s3.amazonaws.com/uifaces/faces/twitter/arashmil/128.jpg" alt="Picture"><p>'+data[key].message+'</p></div>' );

                        }

                    }

                }
            }else{
                var val = $('.chat_count').attr('name');
                $('.chat_count').attr('name', Number(val) +1);
                $('.chat_count').html(Number(val) +1);
            }

    }



    $(document).ready(function() {

        // var pusher = new Pusher('e58ad6b20fa2334decef');
        // var channel = pusher.subscribe('APPL');

        $.ajax({
            url: '{{ env('APP_URL') }}'+'/battle-buddy-get',
            method: "GET",
                success: function( response ) {
                var array = response.data;
                var avatar = response.avatars;
                var htmlStr = "";
                if (array.length > 0) {
                    for (let i = 0; i < array.length; i++) {
                        var avatar = avatar[i];
                        var voiceChat = array[i].voice_chat ? "Yes" : "No";
                        var battleBuddyNumber = i+1;

                        if(Number($('body').attr('user-id')) !== array[i].user_id){
                            htmlStr += "<div></div><div class='forum-post'><div class='forum-header'><div><a href='/user/profile/my-feeds/" + array[i].user_id + "'><img src='" + avatar + "'></a></div><div><h2 class='forum-title'><a href='/user/profile/my-feeds/" + array[i].user_id + "'>" + array[i].username + "</a></h2></div><div>#" + battleBuddyNumber +" <span>" + jQuery.timeago(array[i].created_at) + "</span>     <a name="+ array[i].user_id +';;;'+ array[i].username  +" id='message' onclick='chatEnable("+array[i].user_id+ ',' + '"'+ array[i].username + '"'+" )'>chat</a>   <a class='chat_count' name='0'>0</a>    </div></div><div class='forum-body'><h5 class='m-b-20'>" + array[i].game_name + "&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;" + array[i].platform + "&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;" + array[i].language + "&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;" + array[i].region + "&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;VoiceChat : " + voiceChat + "</h5><p>" + array[i].message + "</p></div></div></div>";
                        }else{

                            htmlStr += "<div></div><div class='forum-post'><div class='forum-header'><div><a href='/user/profile/my-feeds/" + array[i].user_id + "'><img src='" + avatar + "'></a></div><div><h2 class='forum-title'><a href='/user/profile/my-feeds/" + array[i].user_id + "'>" + array[i].username + "</a></h2></div><div>#" + battleBuddyNumber + "<span>" + jQuery.timeago(array[i].created_at) + "</span>    </div></div><div class='forum-body'><h5 class='m-b-20'>" + array[i].game_name + "&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;" + array[i].platform + "&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;" + array[i].language + "&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;" + array[i].region + "&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;VoiceChat : " + voiceChat + "</h5><p>" + array[i].message + "</p></div></div></div>";

                        }

                    }
                } else {
                    htmlStr = "<div>No LFG Entries for the given filter, make one!</div>"
                }
                // $("#post-container").remove();
                document.getElementById('post-container').innerHTML = htmlStr;
            }
        });


        var $chatbox = $('.chatbox'),
            $chatboxTitle = $('.chatbox__title'),
            $chatboxTitleClose = $('.chatbox__title__close'),
            $chatboxCredentials = $('.chatbox__credentials');
        $chatboxTitle.on('click', function() {
            $chatbox.toggleClass('chatbox--tray');
        });
        $chatboxTitleClose.on('click', function(e) {
            e.stopPropagation();
            $chatbox.addClass('chatbox--closed');
        });
        $chatbox.on('transitionend', function() {
            if ($chatbox.hasClass('chatbox--closed')) $chatbox.remove();
        });
        $chatboxCredentials.on('submit', function(e) {
            e.preventDefault();
            $chatbox.removeClass('chatbox--empty');
        });



        $('.chatbox__message').on('keypress',function(e) {
            if(e.which == 13) {
                console.log( $('body'));

                var data = {"from" : $('body').attr('user-id') , "to" : $('#chatbox__title')[0].name , "message" : $('.chatbox__message').val().toString().trim() , "name" :$('#chatbox__title').html()  , "sender" : $('body').attr('user-name') };
                console.log(data);
                $('.chatbox__message').val('');
            $.ajax({
            url: '{{ env('APP_URL') }}'+'/sender',
            method: "POST",
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function( response ) {
               // in success
            }
        });
                return false;
             }
        });


    });
</script>
<script>
function newPost(){
    console.log('came hear');
    $.ajax({
            url: '{{ env('APP_URL') }}'+'/battle-buddy-get',
            method: "GET",
                success: function( response ) {
                var array = response.data;
                var htmlStr = "";
                if(array.length > 0) {
                    for (let i = 0; i < array.length; i++) {

                        var voiceChat = array[i].voice_chat ? "Yes" : "No";

                        if(Number($('body').attr('user-id')) !== array[i].user_id){
                            htmlStr += "<div></div><div class='forum-post'><div class='forum-header'><div><a href='#'><img src='/img/user/user-3.jpg'></a></div><div><h2 class='forum-title'><a href='#'>" + array[i].username + "</a></h2></div><div>#1<span>" + jQuery.timeago(array[i].created_at) + "</span>     <a name="+ array[i].user_id +';;;'+ array[i].username  +" id='message' onclick='chatEnable("+array[i].user_id+ ',' + '"'+ array[i].username + '"'+" )'>chat</a>   <a class='chat_count' name='0'>0</a>    </div></div><div class='forum-body'><h5 class='m-b-20'>" + array[i].game_name + "&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;" + array[i].platform + "&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;" + array[i].language + "&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;" + array[i].region + "&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;VoiceChat : " + voiceChat + "</h5><p>" + array[i].message + "</p></div></div></div>";
                        }else{
                            htmlStr += "<div></div><div class='forum-post'><div class='forum-header'><div><a href='#'><img src='/img/user/user-3.jpg'></a></div><div><h2 class='forum-title'><a href='#'>" + array[i].username + "</a></h2></div><div>#1<span>" + jQuery.timeago(array[i].created_at) + "</span>    </div></div><div class='forum-body'><h5 class='m-b-20'>" + array[i].game_name + "&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;" + array[i].platform + "&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;" + array[i].language + "&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;" + array[i].region + "&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;VoiceChat : " + voiceChat + "</h5><p>" + array[i].message + "</p></div></div></div>";
                        }

                    }
                } else {
                    htmlStr = "<div>No LFG Entries for the given filter, make one!</div>"
                }
                // $("#post-container").remove();
                document.getElementById('post-container').innerHTML = htmlStr;
            }
        });
}
</script>


 <script>

// Enable pusher logging - don't include this in production

window.localStorage.removeItem('data');
localStorage.clear();
Pusher.logToConsole = true;
// window.localStorage.setItem('user', 'tharindu');
console.log('body' , $('body').attr('user-id'));

var pusher = new Pusher('e58ad6b20fa2334decef', {
  cluster: 'ap2',
  forceTLS: true
});

var channel = pusher.subscribe('my-channel');

// window.localStorage.setItem('data', '{}');
    // <a name="+ array[i].user_id +';;;'+ array[i].username  +" id='message' onclick='chatEnable("+array[i].user_id+ ',' + '"'+ array[i].username + '"'+" )'>chat</a>   <a class='chat_count' name='0'>0</a>
channel.bind('test', function(message) {

    if(message.text === "new-post"){
        console.log("naw post came");
        newPost();
        return;
    }
    var json =JSON.parse( JSON.parse(JSON.stringify(message.text)).replace(/'/g , '"'));


    if($('body').attr('user-id') !== ''){
        if($('body').attr('user-id') === json.to){

       var data = window.localStorage.getItem(json.from);
       if(data === null){

           var obj = [ {"type" : "receve" , "message" : json.message} ] ;
           window.localStorage.setItem(json.from , JSON.stringify(obj));
           console.log("chat",json);
           // $('#chatPanel').append("<a name="+ json.from +';;;'+ json.name  +" id='message' onclick='chatEnable("+json.from+ ',' + '"'+ json.sender + '"'+" )'>"+json.sender+"</a>   <a class='chat_count' name='0'>0</a>");
            $('#chatPanel').append(" <div onclick='chatEnable("+json.from+ ',' + '"'+ json.sender + '"'+" )' name="+ json.from +';;;'+ json.name  +" class=\"chat-user\">\n" +
                "                                      <span class=\"chat_count badge badge-danger float-right\" name='0'>0</span>\n" +
                "                                        <img class=\"chat-avatar\" src=\"img/chatList/a2.jpg\" alt=\"\" >\n" +
                "                                        <div class=\"chat-user-name\">\n" +
                "                                            <a href=\"#\">"+json.sender+"</a>\n" +
                "                                        </div>\n" +
                "                                    </div>")

       }else{

           var obj = JSON.parse(data);
           var newMessage = {"type" : "receve" , "message" : json.message} ;
           obj.push(newMessage);
           window.localStorage.setItem(json.from , JSON.stringify(obj));

       }

       window.localStorage.setItem('data' , data);
       chatBoxRebuild(1 , json.from);

   }else if($('body').attr('user-id') === json.from){

       var data = window.localStorage.getItem(json.to);
       if(data === null){
           var obj = [ {"type" : "send" , "message" : json.message} ] ;
           window.localStorage.setItem(json.to , JSON.stringify(obj));

       }else{
           var obj = JSON.parse(data);
           var newMessage = {"type" : "send" , "message" : json.message} ;
           obj.push(newMessage);
           window.localStorage.setItem(json.to , JSON.stringify(obj));
       }

       window.localStorage.setItem('data' , data);
       chatBoxRebuild(1 , json.to);
   }
}

});
</script>

{{-- chatList pop-up message script start --}}
{{--<script src="http://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="--}}
{{--    crossorigin="anonymous"></script>--}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"--}}
{{--    crossorigin="anonymous"></script>--}}
<script>
// GET IN TOUCH COMPONENT
const floating_btn = document.querySelector('.floating-btn');
const close_btn = document.querySelector('.close-btn');
const social_panel_container = document.querySelector('.social-panel-container');

floating_btn.addEventListener('click', () => {
    social_panel_container.classList.toggle('visible')
});

close_btn.addEventListener('click', () => {
    social_panel_container.classList.remove('visible')
});
</script>
{{-- chatList pop-up message script end --}}
@endsection
