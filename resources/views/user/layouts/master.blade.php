<!DOCTYPE html>
<html lang="en">
<head>
  <!-- meta -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>BattleBuddy</title>
  <!-- vendor css -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
  <link rel="stylesheet" href="/plugins/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="/plugins/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="/plugins/animate/animate.min.css">
  <!-- theme css -->
  <link rel="stylesheet" href="/css/theme.css">
  <link rel="stylesheet" href="/css/custom.css">
  @yield('page-css')
  @yield('css')
</head>
<body  class="fixed-header" user-id = {{ Auth::id() }} user-name = {{ Auth::id() ? Auth::user()->username : null }}>

<!-- header -->
<header id="header">
  <div class="container">
    <div class="navbar-backdrop">
      <div class="navbar">
        <div class="navbar-left">
          <a class="navbar-toggle"><i class="fa fa-bars"></i></a>
          <a href="/" class="logo"><img  style="width: 74px" src="/img/logo.png" alt="Gameforest - Game Theme HTML"></a>
          <nav class="nav">
            <ul>
              <li class="">
                <a href="/">Home</a>
              </li>

              <li class="mega-menu mega-games">
                <a href="/battle-buddy">Battle Buddy</a>
              </li>

              <li class="">
                <a href="/feeds">Feed</a>
              </li>

              <li><a href="/forums">Forum</a></li>
            </ul>
          </nav>
        </div>
        <div class="nav navbar-right">
          <ul>
            <!--Hide-->
            @if (!Auth::check())
            <li><a href="/login"><i class="fa sign-in-alt"></i>Login</a>/<a href="/register"><i class="fa sign-in-alt"></i>Register</a></li>
            @else
            <li class="dropdown dropdown-profile">
              <a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false"><img @if (Auth::user()->avatar == 1)
                src="/img/user/avatar.png"
              @else
                src="/storage/{{ Auth::user()->avatar }}"
              @endif alt=""> <span>{{ ucwords(Auth::user()->username) }}</span></a>
              <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item active" href="/user/profile/my-feeds"><i class="fa fa-user"></i> Profile</a>
                <a class="dropdown-item" href="/user/profile/change-account-settings"><i class="fa fa-cog"></i> Settings</a>
                <a class="dropdown-item" href="/logout"><i class="fa fa-sign-out"></i> Logout</a>
              </div>
            </li>
            @endif
            <li><a data-toggle="search"><i class="fa fa-search"></i></a></li>


          </ul>
        </div>
      </div>
    </div>


    <div class="navbar-search">
      <div class="container">
        <form method="post" action="/">
          @csrf
          <input type="text" class="form-control" placeholder="Search...">
          <i class="fa fa-times close"></i>
        </form>
      </div>
    </div>


  </div>
</header>
<!-- /header -->

@yield('content')


  <!-- vendor js -->
  <script src="/plugins/jquery/jquery-3.2.1.min.js"></script>
  <script src="/plugins/popper/popper.min.js"></script>
  <script src="/plugins/bootstrap/js/bootstrap.min.js"></script>

  <!-- plugins js -->
  <script src="/plugins/easypiechart/jquery.easing.1.3.js"></script>
  <script src="/plugins/easypiechart/jquery.easypiechart.min.js"></script>
  <script>
    (function($) {
      "use strict";
      // easyPieChart
      $('.chart').easyPieChart({
        barColor: '#5eb404',
        trackColor: '#e3e3e3',
        easing: 'easeOutBounce',
        onStep: function(from, to, percent) {
          $(this.el).find('span').text(Math.round(percent));
        }
      });
      $('.search-game, .navbar-search .form-control').keyup(function() {
        var search = $(this).val().toLowerCase();
        $.each($('.card-title'), function() {
          if ($(this).text().toLowerCase().indexOf(search) === -1) {
            $(this).parent().parent().parent().hide();
          } else {
            $(this).parent().parent().parent().show();
          }
        });
      });
    })(jQuery);
  </script>

  <!-- theme js -->
   <script src="/js/theme.min.js"></script>

  @yield('page-scripts')
  @yield('js')
</body>
</html>
