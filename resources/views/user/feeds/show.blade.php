@extends('user.layouts.master')

@section ('content')

<!-- main -->
<section class="breadcrumbs">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="/">Home</a></li>
            <li><a href="/feeds">Feeds</a></li>
            <li class="active">{{ ucwords($feed[0]->name) }}</li>
        </ol>
    </div>
</section>

  <section class="bg-image" style="background-image: url('https://img.youtube.com/vi/zSd9McRXHZ8/maxresdefault.jpg');">
    <div class="overlay-light"></div>
    <div class="container">
      <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src="{{ $feed[0]->video_url }}?rel=0&amp;amp;&amp;amp;showinfo=0" allowfullscreen></iframe>
      </div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="row">
        <div class="col-lg-9">
          <div class="post post-single">
            <div class="post-header">
              <div>
                <a href="/feeds/user/{{ $feed[0]->user_id }}"><img @if (Auth::user()->avatar == 1) src="/img/user/avatar.png" @else src="/storage/{{ Auth::user()->avatar }}" @endif alt=""></a>
              </div>
              <div>
                <h2 class="post-title">{{ $feed[0]->name }}</h2>
                <div class="post-meta">
                  <span><i class="fa fa-clock-o"></i> {{ $feed[0]->created_at->diffForHumans() }} by <a href="/feeds/user/{{ $feed[0]->user_id }}">{{ $feed[0]->username }}</a></span>
                  <span><a><i class="fa fa-comment-o"></i> {{ $commentCount }} comments</a></span>
                  <span><a id="{{ $feed[0]->id }}" @if ($feed[0]->status == 1) class="text-primary post-like" @else class="post-like" @endif><i @if ($feed[0]->status == 1) class="fa fa-heart text-primary" @endif class="fa fa-heart-o"></i> Like</a></span>
                  <span class="text-primary">@if ($feed[0]->like_count == null) 0 likes @else {{ $feed[0]->like_count }} likes @endif</span>
                </div>
              </div>
            </div>
            <p>{{ $feed[0]->description }}</p>
          </div>
          <div class="post-actions">
            <div class="post-tags">
                @foreach($tags as $tag)
                    <a>#{{ trim($tag) }}</a>
                @endforeach
            </div>

            {{-- <div class="social-share">
              <a class="btn btn-social btn-facebook btn-circle" href="#" data-toggle="tooltip" title="" data-placement="bottom" role="button" data-original-title="Share on facebook"><i class="fa fa-facebook"></i></a>
              <span>5.345k</span>
              <a class="btn btn-social btn-twitter btn-circle" href="#" data-toggle="tooltip" title="" data-placement="bottom" role="button" data-original-title="Share on twitter"><i class="fa fa-twitter"></i></a>
              <a class="btn btn-social btn-google-plus btn-circle" href="#" data-toggle="tooltip" title="" data-placement="bottom" role="button" data-original-title="Share on google-plus"><i class="fa fa-google-plus"></i></a>
            </div> --}}

          </div>
          <div id="comments" class="comments anchor">
            <div class="comments-header">
              <h5><i class="fa fa-comment-o"></i> Comments ({{ $commentCount }})</h5>

              {{-- <div class="dropdown float-right">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="comment-filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Sorted by <span class="caret"></span></button>
                <div class="dropdown-menu dropdown-menu-right">
                  <a filter="best" class="filter dropdown-item active" feedId="{{ $feed[0]->id }}">Best</a>
                  <a filter="oldest" class="filter dropdown-item" feedId="{{ $feed[0]->id }}">Oldest</a>
                  <a filter="random" class="filter dropdown-item" feedId="{{ $feed[0]->id }}">Random</a>
                </div>
              </div> --}}

            </div>
            {{-- <a class="btn btn-primary text-left m-t-15 btn-block" href="#comments" role="button"><i class="fa fa-spinner fa-pulse m-r-5"></i> Load more 4 comments</a> --}}

            <ul id="comment-container">
              @foreach ($comments as $comment)
              <li>
                <div id="com{{ $comment->id }}" class="comment">
                  <div class="comment-avatar"><a href="/feeds/user/{{ $comment->user_id }}"><img @if ($comment->avatar == 1) src="/img/user/avatar.png" @else src="/storage/{{ $comment->avatar }}" @endif></a></div>
                  <div class="comment-post">
                    <div id="commentHeader{{ $comment->id }}" class="comment-header">
                      <div class="comment-author">
                        <h5>
                          <a href="/feeds/user/{{ $comment->user_id }}">{{ $comment->username }}</a>
                          <span class="badge badge-outline-dark">{{ ucwords($comment->role) }}</span>
                        </h5>
                      </div>
                      <div class="comment-action">
                        <div class="comment-action">
                          <div class="dropdown float-right">
                            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-chevron-down"></i></a>
                            <div class="dropdown-menu dropdown-menu-right">
                              <a id="spam{{ $comment->id }}" spamId="{{ $comment->id }}" class="spam dropdown-item">@if ($comment->is_spam == 1) Mark as not spam @else Mark as spam @endif</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <p id="commentBody{{ $comment->id }}">{{ $comment->message }}</p>
                    <div id="commentFooter{{ $comment->id }}" class="comment-footer">
                      <ul>
                        <li><a id="comment-id" value="{{ $comment->id }}" commentId="{{ $comment->id }}" @if ($comment->status == 1) class="text-primary like" @else class="like" @endif><i @if ($comment->status == 1) class="text-primary fa fa-heart" @else class="fa fa-heart-o" @endif></i> Like</a></li>
                        <li><a class="reply" clicked="no" id="{{ $comment->id }}" commentId="com{{ $comment->id }}"><i class="icon-reply"></i> Reply</a></li>
                        <li><a><i class="fa fa-clock-o"></i> {{ $comment->created_at->diffForHumans() }}</a></li>
                        @if (Auth::user()->id == $comment->user_id)
                        <li id="deleteComment{{ $comment->id }}"><a onclick="deleteComment(this)" id="{{ $comment->id }}"><span class="text-danger"><i class="fa fa-trash"></i> Delete</span></a></li>
                        @endif
                        @if ($comment->have_reply == 1)
                        <li>
                          <a class="load-replies text-primary" id="loadreplies{{ $comment->id }}" comment-id="{{ $comment->id }}">
                            <i class="fa fa-spinner fa-pulse m-r-5"></i> Load replies
                          </a>
                        </li>
                        @endif
                      </ul>
                    </div>
                  </div>
                </div>
                
                <ul id="replyContainer{{ $comment->id }}">
                  
                </ul>
              </li>
              @endforeach
            </ul>

            <form id="comment">
              <h5>Leave a comment</h5>
              <div class="form-group">
                <textarea id="message" class="form-control" rows="5" placeholder="Your Comment"></textarea>
                <input type="hidden" value="{{ $feed[0]->id }}" id="feed-id">
              </div>
              <span class="text-danger">{{ $errors->first('message') }}</span>
              <button type="button" onclick="sendComment()" id="send_form" class="btn btn-primary btn-shadow" role="button">Submit Comment</button>
            </form>
            @include ('user.layouts.errors')
          </div>
        </div>
        <div class="col-lg-3">
          <div class="sidebar">
            <!-- widget post  -->
            <div class="widget widget-videos">
              <h5 class="widget-title">Top rated feeds</h5>
              @if (!empty($popularFeeds))
                @foreach ($popularFeeds as $popularFeed)
                    @if (!is_null($popularFeed->image_path))
                    <a href="/feeds/show/{{ $popularFeed->id }}">
                    <img src="/storage/{{ $popularFeed->image_path }}" alt="/storage/{{ $popularFeed->name }}">
                    </a>
                    @endif
                    @if (!is_null($popularFeed->video_url))
                    <a href="/feeds/show/{{ $popularFeed->id }}">
                        <h6 class="post-title">
                            <h5 class="post-title"><a href="/feeds/show/{{ $popularFeed->id }}">{{ $popularFeed->name }}</a></h5>
                        </h6>
                        <iframe width="770" height="200" src="{{ $popularFeed->video_url }}" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"></iframe>
                    </a>
                    @endif
                @endforeach
              @endif
            </div>
            <!-- /widget post -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /main -->

@endsection

@section('page-scripts')
{{-- jquery validation cdn --}}
<script src="/js/jquery.timeago.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js" integrity="sha256-xLhce0FUawd11QSwrvXSwST0oHhOolNoH9cUXAcsIAg=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js" integrity="sha256-vb+6VObiUIaoRuSusdLRWtXs/ewuz62LgVXg2f1ZXGo=" crossorigin="anonymous"></script>
<script src="/js/comment-reply.js" type="text/javascript"></script>
<script src="/js/like-dislike.js" type="text/javascript"></script>
<script src="/js/feed-like-dislike.js" type="text/javascript"></script>
<script src="/js/mark-as-spam.js" type="text/javascript"></script>
<script src="/js/filter-comments.js" type="text/javascript"></script>
<script src="/js/edit-delete-comments.js" type="text/javascript"></script>
<script>
    $(".load-replies").on("click", function(event) {
        event.preventDefault();
        var commentId = this.getAttribute("comment-id");
        var data = {
            "commentId":commentId
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: 'http://127.0.0.1:8000/feeds/comments/reply/load',
            method: "POST",
            data: data,
                success: function( response ) {
                    var parentCommentId = commentId;
                    commentId = "#replyContainer" + commentId;
                    var replies = "";
                    for (let i = 0; i < response.data.length; i++) {
                        if (response.data[i].avatar == "1") { var avatar = "/img/user/avatar.png"; } else { var avatar = "/storage/" + response.data[i].avatar; }
                        if (response.data[i].status == "1") { var like = "fa fa-heart text-primary"; } else { var like = "fa fa-heart-o"; }
                        if (response.data[i].is_spam == 1) { var spamStatus = "Mark as not spam"; } else { var spamStatus = "Mark as spam"; }
                        if (response.data[i].user_id == response.userId) {
                            var deleteComment = "<li id='deleteComment" + response.data[i].comment_id + "'><a onclick='deleteComment(this)' id='" + response.data[i].comment_id + "'><span class='text-danger'><i class='fa fa-trash'></i> Delete</span></a></li>"; 
                        } else {
                            var deleteComment = ""; 
                        }
                        replies += "<li><div id='com" + response.data[i].comment_id + "' class='comment mb-3'><div class='comment-avatar'><a href='/user/profile/my-feeds'><img src=" + avatar + "></a></div><div class='comment-post'><div class='comment-header'><div class='comment-author'><h5><a href='/user/profile/my-feeds'>" + response.data[i].username + "</a><span class='badge badge-outline-dark'>" + response.data[i].role + "</span></h5></div><div class='comment-action'><div class='comment-action'><div class='dropdown float-right'><a href='#' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='fa fa-chevron-down'></i></a><div class='dropdown-menu dropdown-menu-right'><a id='spam" + response.data[i].comment_id + "' class='dropdown-item' onclick='spam(this)' spamId='" + response.data[i].comment_id + "'>" + spamStatus + "</a></div></div></div></div></div><p>" + response.data[i].message + "</p><div class='comment-footer'><ul><li><a commentId='" + response.data[i].comment_id + "' class='like' onclick='like(this)'><i class='" + like + "'></i> Like</a></li><li><a class='reply' superComment='" + parentCommentId + "' clicked='no' id='" + response.data[i].comment_id + "' commentId='com" + response.data[i].comment_id + "' onclick='reply(this)'><i class='icon-reply'></i> Reply</a></li><li><a><i class='fa fa-clock-o'></i> " + jQuery.timeago(response.data[i].created_at) + "</a></li>" + deleteComment + "</ul></div></div></div></li>";
                    }
                    $(commentId).append(replies);
                    
                    // hiding the load replies button after complete loading replies
                    $("#loadreplies"+parentCommentId).hide();
            }
        });
    });

    function loadReplies(event) {
        var commentId = event.getAttribute("comment-id");
        var data = {
            "commentId":commentId
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: 'http://127.0.0.1:8000/feeds/comments/reply/load',
            method: "POST",
            data: data,
                success: function( response ) {
                    var parentCommentId = commentId;
                    commentId = "#replyContainer" + commentId;
                    var replies = "";
                    for (let i = 0; i < response.data.length; i++) {
                        if (response.data[i].avatar == "1") { var avatar = "/img/user/avatar.png"; } else { var avatar = "/storage/" + response.data[i].avatar; }
                        if (response.data[i].status == "1") { var like = "fa fa-heart text-primary"; } else { var like = "fa fa-heart-o"; }
                        if (response.data[i].is_spam == 1) { var spam = "Mark as not spam"; } else { var spam = "Mark as spam"; }
                        if (response.data[i].user_id == response.userId) {
                            var deleteComment = "<li id='deleteComment" + response.data[i].comment_id + "'><a onclick='deleteComment(this)' id='" + response.data[i].comment_id + "'><span class='badge badge-pill badge-danger'><i class='fa fa-trash'></i> Delete</span></a></li>"; 
                        } else {
                            var deleteComment = ""; 
                        }
                        replies += "<li><div id='com" + response.data[i].comment_id + "' class='comment'><div class='comment-avatar'><a href='/user/profile/my-feeds'><img src=" + avatar + "></a></div><div class='comment-post'><div class='comment-header'><div class='comment-author'><h5><a href='/user/profile/my-feeds'>" + response.data[i].username + "</a></h5></div><div class='comment-action'><div class='comment-action'><div class='dropdown float-right'><a href='#' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='fa fa-chevron-down'></i></a><div class='dropdown-menu dropdown-menu-right'><a id='spam" + response.data[i].comment_id + "' spamId='" + response.data[i].comment_id + "' class='spam dropdown-item' onclick='spam(this)'>" + spam + "</a></div></div></div></div></div><p>" + response.data[i].message + "</p><div class='comment-footer'><ul><li><a commentId='" + response.data[i].comment_id + "' class='like' onclick='like(this)'><i class='" + like + "'></i> Like</a></li><li><a class='reply' superComment='" + parentCommentId + "' clicked='no' id='" + response.data[i].comment_id + "' commentId='com" + response.data[i].comment_id + "' onclick='reply(this)'><i class='icon-reply'></i> Reply</a></li><li><a><i class='fa fa-clock-o'></i> " + jQuery.timeago(response.data[i].created_at) + "</a></li>" + deleteComment + "</ul></div></div></div></li>";
                    }
                    $(commentId).append(replies);
                    
                    // hiding the load replies button after complete loading replies
                    $("#loadreplies"+parentCommentId).hide();
            }
        });
    }
</script>
@endsection
