@extends('user.layouts.master')

@section ('content')
    <section class="" data-fixed="true">
    <div class="container">
        <h2>Upload Content Here</h2>

        <form action="/feeds/create" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="Enter name here">
            </div>

            <div class="form-group">
              <label for="gamer_id">Gamer ID</label>
              <input type="text" class="form-control" id="gamer_id" name="gamer_id"  value="{{ old('name') }}" placeholder="Gamer ID">
            </div>

            <div class="form-group">
                <label for="tags">Tags</label>
                <input type="text" class="form-control" id="tags" name="tags"  value="{{ old('name') }}" placeholder="Tags">
            </div>

            <div class="form-group">
                <label for="content">Content &nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input ml-0" type="radio" name="content" id="video" value="Video" onclick="videoArea()">
                        <label style="margin-right: 20px" class="form-check-label" for="video">Video</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input ml-0" type="radio" name="content" id="image" value="Image" onclick="imageArea()">
                        <label class="form-check-label" for="image">Image</label>
                    </div>
            </div>

            <div id="video_url" class="form-group" style="display: none">
                <label for="video_placeholder">Video URL</label>
                <input type="text" class="form-control" id="video_placeholder" name="video_placeholder" placeholder="Enter the URL">
            </div>

            <div id="images" class="form-group" style="display:none">
                <label for="image_placeholder">Upload an image</label>
                <input type="file" class="form-control-file" id="image_placeholder" accept="image/*" name="image_placeholder" placeholder="Upload an image">
            </div>

            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control" id="description" rows="3" name="description" value="{{ old('description') }}"></textarea>
            </div>

            <div class="form-group form-check">
              <input type="checkbox" class="form-check-input ml-0" id="add_to_battlebuddy" value="on" name="add_to_battlebuddy">
              <label class="form-check-label" for="add_to_battlebuddy">Do you want to add this video to the battlebuddy to play competition<span class="text-primary"> (Comming Soon)</span></label>
            </div>
            <div class="form-group form-check">
                <input type="checkbox" class="form-check-input ml-0" id="owner" name="is_owner" value="on">
                <label class="form-check-label" for="owner">Are you the owner of the content</label>
            </div>
            <div class="form-group form-check">
                <input type="checkbox" class="form-check-input ml-0" id="terms_and_conditions" name="terms_and_conditions" value="1">
                <label class="form-check-label" for="terms_and_conditions">I agree to the terms and conditions</label>
            </div>
            <button type="submit" class="btn btn-primary">Upload</button>
          </form>

    </div>
    </section>
@endsection
@section('page-scripts')
{{--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script>
    function videoArea() {
        var videoPlaceholder = document.getElementById("video_url");
        var imagePlaceholder = document.getElementById("images");
        if (videoPlaceholder.style.display === "none") {
            videoPlaceholder.style.display = "block";
            imagePlaceholder.style.display = "none";
        } else {
            videoPlaceholder.style.display = "none";
        }
    }

    function imageArea() {
        var imagePlaceholder = document.getElementById("images");
        var videoPlaceholder = document.getElementById("video_url");
        if (imagePlaceholder.style.display === "none") {
            imagePlaceholder.style.display = "block";
            videoPlaceholder.style.display = "none";
        } else {
            imagePlaceholder.style.display = "none";
        }
    }
</script>

@endsection


