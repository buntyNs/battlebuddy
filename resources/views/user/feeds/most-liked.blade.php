@extends('user.layouts.master')

@section ('content')

  <!-- main -->
  <section class="toolbar toolbar-links" data-fixed="true">
      <div class="container">
            <div class="row">
                <div class="col-lg-2"></div>
                <div  class="col-lg-8">
                    <button id="send_search" class="btn btn-warning" type="button" aria-haspopup="true" onclick="allFeeds()" aria-expanded="true" style=" align-self: center">All Feeds</button>
                    <button id="send_search" class="btn btn-warning" type="button" aria-haspopup="true" onclick="latestFeeds()" aria-expanded="true" style=" align-self: center">New</button>
                    <button id="best-voted" class="btn btn-warning" type="button" aria-haspopup="true" onclick="bestVoted()" aria-expanded="true" style=" align-self: center">Top Plays</button>
                    <button id="send_search" class="btn btn-warning" type="button" aria-haspopup="true" onclick="addFeed()" aria-expanded="true" style=" align-self: center;float: right">+</button>
                </div>
            </div>
      </div>
  </section>
  <section>
    <div class="container">
      <div class="row">
          <div class="col-lg-2"></div>
        <div id="" class="col-lg-8">
          <!-- post -->
          @if (isset($mostLikedFeeds))
                <div class="infinite-scroll">
            @foreach ($mostLikedFeeds as $mostLikedFeed)
                <div class="post">
                    <h2 class="post-title"><a href="/feeds/show/{{ $mostLikedFeed->id }}">{{ $mostLikedFeed->name }}</a></h2>
                    <div class="post-meta">
                        <span><i class="fa fa-clock-o"></i> {{ $mostLikedFeed->created_at }} by <a href="/user/profile/my-feeds">{{ $mostLikedFeed->username }}</a></span>
                        @if ($mostLikedFeed->like_count != null || $mostLikedFeed->like_count != 0) <span><a><i class="fa fa-heart"></i> {{ $mostLikedFeed->like_count }} likes</a></span> @endif
                    </div>
                    <div class="post-thumbnail">
                        @if (!is_null($mostLikedFeed->image_path))
                        <img src="/storage/{{ $mostLikedFeed->image_path }}" alt="/storage/{{ $mostLikedFeed->name }}">
                        @endif
                        @if (!is_null($mostLikedFeed->video_url))
                        <iframe width="853" height="480" src="{{ $mostLikedFeed->video_url }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        @endif
                    </div>
                    <p>{{ $mostLikedFeed->description }}</p>
                </div>
            @endforeach
                    {{$mostLikedFeeds->links()}}
                </div>
          @endif

        </div>
    </div>
  </section>
  <!-- /main -->
@endsection

@section('page-scripts')


<script src="/js/jquery.timeago.js" type="text/javascript"></script>
<script src="/js/jquery.jscroll.min.js" type="text/javascript"></script>
<script>

    $('ul.pagination').hide();
    $(function() {
        $('.infinite-scroll').jscroll({
            autoTrigger: true,
            loadingHtml: '<img style="height: 10px;" class="center-block" src="/img/loading.gif" alt="Loading..." />',
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.infinite-scroll',
            callback: function() {
                $('ul.pagination').remove();
            }
        });
    });

    function addFeed() {
        location.replace("http://127.0.0.1:8000/feeds/create")
    }

    function allFeeds() {
        location.replace("http://127.0.0.1:8000/feeds")
    }

    function latestFeeds() {
        location.replace("http://127.0.0.1:8000/feeds/latest");
    }
</script>
@endsection
