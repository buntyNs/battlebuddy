@extends('user.layouts.master')

@section ('content')

  <!-- main -->
  <section class="toolbar toolbar-links" data-fixed="true">
      <div class="container">
            <div class="row">
                <div class="col-lg-2"></div>
                <div  class="col-lg-8">
                    <button id="send_search" class="btn btn-warning" type="button" aria-haspopup="true" onclick="bestVoted()" aria-expanded="true" style=" align-self: center">Best Voted</button>
                    <button id="send_search" class="btn btn-warning" type="button" aria-haspopup="true" onclick="latestFeeds()" aria-expanded="true" style=" align-self: center">New</button>
                    <button id="best-voted" class="btn btn-warning" type="button" aria-haspopup="true" onclick="bestVoted()" aria-expanded="true" style=" align-self: center">Top Plays</button>
                    <button id="send_search" class="btn btn-warning" type="button" aria-haspopup="true" onclick="addFeed()" aria-expanded="true" style=" align-self: center;float: right">+</button>
                </div>
            </div>
      </div>
  </section>
  <section>
    <div class="container">
      <div class="row">
          <div class="col-lg-2"></div>
        <div id="" class="col-lg-8">
          <!-- post -->
          @if (isset($latestFeeds))
                <div class="infinite-scroll">
            @foreach ($latestFeeds as $feed)
                <div class="post">
                    <h2 class="post-title"><a href="/feeds/show/{{ $feed->id }}">{{ $feed->name }}</a></h2>
                    <div class="post-meta">
                        <span><i class="fa fa-clock-o"></i> {{ $feed->created_at }} by <a href="/user/profile/my-feeds">{{ $feed->username }}</a></span>
                        @if ($feed->like_count != null || $feed->like_count != 0) <span><a><i class="fa fa-heart"></i> {{ $feed->like_count }} likes</a></span> @else <span><a><i class="fa fa-heart-o"></i>0 likes</a></span>@endif
                    </div>
                    <div class="post-thumbnail">
                        @if (!is_null($feed->image_path))
                        <img src="/storage/{{ $feed->image_path }}" alt="/storage/{{ $feed->name }}">
                        @endif
                        @if (!is_null($feed->video_url))
                        <iframe width="853" height="480" src="{{ $feed->video_url }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        @endif
                    </div>
                    <p>{{ $feed->description }}</p>
                </div>
            @endforeach
                    {{$latestFeeds->links()}}
                </div>
          @endif

        </div>



        <!-- sidebar -->
{{--        <div class="col-lg-4">--}}
{{--          <div class="sidebar">--}}
{{--            <!-- widget search -->--}}
{{--            <div class="widget widget-search">--}}
{{--              <form>--}}
{{--                <div class="form-group input-icon-right">--}}
{{--                  <i class="fa fa-search"></i>--}}
{{--                  <input type="text" class="form-control" placeholder="Type to search...">--}}
{{--                </div>--}}
{{--              </form>--}}
{{--            </div>--}}

{{--            <!-- widget post  -->--}}
{{--            <div class="widget widget-post">--}}
{{--              <h5 class="widget-title">Recommends</h5>--}}
{{--              <a href="blog-post.html"><img src="https://i1.ytimg.com/vi/4BLkEJu9szM/mqdefault.jpg" alt=""></a>--}}
{{--              <h4><a href="blog-post.html">Titanfall 2's Trophies Only Have 3 Earned Through Multiplayer</a></h4>--}}
{{--              <span><i class="fa fa-clock-o"></i> June 12, 2017</span>--}}
{{--              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel neque sed ante facilisis efficitur.</p>--}}
{{--            </div>--}}

{{--            <!-- widget facebook -->--}}
{{--            --}}{{-- <div class="widget">--}}
{{--              <h5 class="widget-title">Follow Us on Facebook</h5>--}}
{{--              <div id="fb-root"></div>--}}
{{--              <script async="" src="https://www.google-analytics.com/analytics.js"></script>--}}
{{--              <script id="facebook-jssdk" src="//connect.facebook.net/en_US/sdk.js#xfbml=1&amp;version=v2.8"></script>--}}
{{--              <script>--}}
{{--                (function(d, s, id) {--}}
{{--                  var js, fjs = d.getElementsByTagName(s)[0];--}}
{{--                  if (d.getElementById(id)) return;--}}
{{--                  js = d.createElement(s);--}}
{{--                  js.id = id;--}}
{{--                  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9";--}}
{{--                  fjs.parentNode.insertBefore(js, fjs);--}}
{{--                }(document, 'script', 'facebook-jssdk'));--}}
{{--              </script>--}}
{{--              <div class="fb-page" data-href="https://www.facebook.com/yakuthemes" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>--}}
{{--            </div> --}}

{{--            <!-- widget post -->--}}
{{--            --}}{{-- <div class="widget widget-post">--}}
{{--              <h5 class="widget-title">Popular on Gameforest</h5>--}}
{{--              <a href="blog-post.html"><img src="img/blog/blog-widget-popular-1.jpg" alt=""></a>--}}
{{--              <h4><a href="blog-post.html">Red Dead Redemption Being Modded Into GTA5 Multiplayer</a></h4>--}}
{{--              <span><i class="fa fa-clock-o"></i> June 16, 2017</span>--}}
{{--              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel neque sed anter.</p>--}}
{{--              <ul class="widget-list">--}}
{{--                <li>--}}
{{--                  <div class="widget-img"><a href="blog-post.html"><img src="img/blog/blog-widget-1.jpg" alt=""></a></div>--}}
{{--                  <div>--}}
{{--                    <h4><a href="blog-post.html">Dead Island 2 and Escape Impressions</a></h4>--}}
{{--                    <span>July 16, 2017</span>--}}
{{--                  </div>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                  <div class="widget-img"><a href="blog-post.html"><img src="img/blog/blog-widget-2.jpg" alt=""></a></div>--}}
{{--                  <div>--}}
{{--                    <h4><a href="blog-post.html">How to Finish Mafia 3 With All of Your Underbosses</a></h4>--}}
{{--                    <span>June 29, 2017</span>--}}
{{--                  </div>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                  <div class="widget-img"><a href="blog-post.html"><img src="img/blog/blog-widget-3.jpg" alt=""></a></div>--}}
{{--                  <div>--}}
{{--                    <h4><a href="blog-post.html">Spider-Man Spin-Off, Venom, Gets Release Date</a></h4>--}}
{{--                    <span>June 15, 2017</span>--}}
{{--                  </div>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                  <div class="widget-img"><a href="blog-post.html"><img src="img/blog/blog-widget-4.jpg" alt=""></a></div>--}}
{{--                  <div>--}}
{{--                    <h4><a href="blog-post.html">Is Ghost Recon: Wildlands Worth Your Time?</a></h4>--}}
{{--                    <span>June 13, 2017</span>--}}
{{--                  </div>--}}
{{--                </li>--}}
{{--              </ul>--}}
{{--            </div> --}}

{{--            <!-- widget tabs -->--}}
{{--            <div class="widget widget-tabs">--}}
{{--              <ul class="nav nav-tabs" role="tablist">--}}
{{--                <li class="nav-item"><a class="nav-link active" href="#comments" aria-controls="comments" role="tab" data-toggle="tab"><i class="fa fa-comment-o"></i> Comments</a></li>--}}
{{--                <li class="nav-item"><a class="nav-link" href="#popular" aria-controls="popular" role="tab" data-toggle="tab">Popular</a></li>--}}
{{--                <li class="nav-item"><a class="nav-link" href="#recent" aria-controls="recent" role="tab" data-toggle="tab">Recent</a></li>--}}
{{--              </ul>--}}
{{--              <div class="tab-content">--}}
{{--                <div role="tabpanel" class="tab-pane active" id="comments">--}}
{{--                  <ul class="widget-comments">--}}
{{--                    <li>--}}
{{--                      <div><a href="profile.html"><img src="img/user/user-2.jpg" alt=""></a></div>--}}
{{--                      <div>--}}
{{--                        <a href="blog-post.html#comments"><b>Elizabeth:</b> It would have taken a ridiculous amount of careful precise actions.</a>--}}
{{--                      </div>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                      <div><a href="profile.html"><img src="img/user/user-3.jpg" alt=""></a></div>--}}
{{--                      <div>--}}
{{--                        <a href="blog-post-disqus.html#comments"><b>Clark:</b> Lorem ipsum dolor sit amet, consectetur adipiscing elit curabitur risque.</a>--}}
{{--                      </div>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                      <div><a href="profile.html"><img src="img/user/user-1.jpg" alt=""></a></div>--}}
{{--                      <div>--}}
{{--                        <a href="blog-post-video.html#comments"><b>Venom:</b> Practically no verticality, which on levels like The Spire (Geonosis) incredible.</a>--}}
{{--                      </div>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                      <div><a href="profile.html"><img src="img/user/user-3.jpg" alt=""></a></div>--}}
{{--                      <div>--}}
{{--                        <a href="blog-post-disqus.html#comments"><b>Clark:</b> I'm low level at this point and have almost nothing unlocked, and veteran players have an unfair advantage over me thanks.</a>--}}
{{--                      </div>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                      <div><a href="profile.html"><img src="img/user/user-5.jpg" alt=""></a></div>--}}
{{--                      <div>--}}
{{--                        <a href="blog-post-disqus.html#comments"><b>Trevor:</b> A lot of people would have stopped playing now if it wasn't for the online stuff!</a>--}}
{{--                      </div>--}}
{{--                    </li>--}}
{{--                  </ul>--}}
{{--                </div>--}}
{{--                <div role="tabpanel" class="tab-pane" id="popular">--}}
{{--                  <div class="widget-post">--}}
{{--                    <ul class="widget-list">--}}
{{--                      <li>--}}
{{--                        <img src="https://i1.ytimg.com/vi/B6qY-P4eo1Q/mqdefault.jpg" alt="">--}}
{{--                        <h4><a href="blog-post.html">How to Finish Mafia 3 With All of Your Underbosses</a></h4>--}}
{{--                        <span><i class="fa fa-clock-o"></i> July 12, 2017</span>--}}
{{--                        <span>19 comments</span>--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel neque sed anter.</p>--}}
{{--                      </li>--}}
{{--                      <li>--}}
{{--                        <h4><a href="blog-post.html">Uncharted: The Lost Legacy's Demo</a></h4>--}}
{{--                        <span>June 28, 2017</span>--}}
{{--                        <span>41 comments</span>--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel neque sed anter.</p>--}}
{{--                      </li>--}}
{{--                      <li>--}}
{{--                        <h4><a href="blog-post.html">Mafia III Stones Unturned DLC Launch Trailer</a></h4>--}}
{{--                        <span>June 17, 2017</span>--}}
{{--                        <span>7 comments</span>--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel neque sed anter.</p>--}}
{{--                      </li>--}}
{{--                    </ul>--}}
{{--                  </div>--}}
{{--                </div>--}}
{{--                <div role="tabpanel" class="tab-pane" id="recent">--}}
{{--                  <div class="widget-post">--}}
{{--                    <ul class="widget-list">--}}
{{--                      <li>--}}
{{--                        <img src="https://i1.ytimg.com/vi/ckUrcdnWZ2g/mqdefault.jpg" alt="">--}}
{{--                        <h4><a href="blog-post.html">Free Mass Effect: Andromeda Trial Now Available On All Platforms</a></h4>--}}
{{--                        <span><i class="fa fa-clock-o"></i> July 12, 2017</span>--}}
{{--                        <span>76 comments</span>--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel neque sed anter.</p>--}}
{{--                      </li>--}}
{{--                      <li>--}}
{{--                        <h4><a href="blog-post.html">GTA 5 Online Players Find Secret Alien Mission</a></h4>--}}
{{--                        <span>June 23, 2017</span>--}}
{{--                        <span>34 comments</span>--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel neque sed anter.</p>--}}
{{--                      </li>--}}
{{--                      <li>--}}
{{--                        <h4><a href="blog-post.html">Mafia III Stones Unturned DLC Launch Trailer</a></h4>--}}
{{--                        <span>June 17, 2017</span>--}}
{{--                        <span>7 comments</span>--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel neque sed anter.</p>--}}
{{--                      </li>--}}
{{--                    </ul>--}}
{{--                  </div>--}}
{{--                </div>--}}
{{--              </div>--}}
{{--            </div>--}}

{{--            <!-- widget tags -->--}}
{{--            <div class="widget widget-tags">--}}
{{--              <h5 class="widget-title">Popular Tags</h5>--}}
{{--              <div class="post-tags">--}}
{{--                <a href="#">#playstation 4</a>--}}
{{--                <a href="#">#remastered</a>--}}
{{--                <a href="#">#uncharted</a>--}}
{{--                <a href="#">#game</a>--}}
{{--                <a href="#">#blood and wine</a>--}}
{{--                <a href="#">#games</a>--}}
{{--              </div>--}}
{{--            </div>--}}
{{--          </div>--}}
{{--        </div>--}}
{{--      </div>--}}
    </div>
  </section>
  <!-- /main -->
@endsection

@section('page-scripts')


<script src="/js/jquery.timeago.js" type="text/javascript"></script>
<script src="/js/jquery.jscroll.min.js" type="text/javascript"></script>
<script>

    $('ul.pagination').hide();
    $(function() {
        $('.infinite-scroll').jscroll({
            autoTrigger: true,
            loadingHtml: '<img style="height: 10px;" class="center-block" src="/img/loading.gif" alt="Loading..." />',
            padding: 0,
            nextSelector: '.pagination li.active + li a',
            contentSelector: 'div.infinite-scroll',
            callback: function() {
                $('ul.pagination').remove();
            }
        });
    });

    function addFeed() {
        var url_create = "{{ env('APP_URL') }}"+"/feeds/create"
        location.replace(url_create);
    }

    function bestVoted() {
        location.replace("{{ env('APP_URL') }}"+"/feeds/best-voted");
    }

    function latestFeeds() {
        location.replace("{{ env('APP_URL') }}"+"/feeds/latest");
    }

    // $(document).ready(function() {
    //     page = 1;
    //     loadPage(page)
    // });
    //
    // $(window).scroll(function() { //detect page scroll
    //     if($(window).scrollTop() + $(window).height() >= $(document).height()) { //if user scrolled from top to bottom of the page
    //         page++; //page number increment
    //         loadPage(page); //load content
    //     }
    // });
    // function loadPage(page){
    //     $.ajax({
    //         url: 'http://127.0.0.1:8000/feeds-get?page='+page,
    //         method: "GET",
    //         success: function( response ) {
    //             console.log(response);
    //             var array = response.data.data;
    //             var htmlStr = "";
    //             if(array.length > 0) {
    //                 for (let i = 0; i < array.length; i++) {
    //                     var addToBattlebuddy = array[i].add_to_battlebuddy ? "Yes" : "No";
    //                     var isOwner = array[i].is_owner ? "Yes" : "No";
    //                     if (array[i].video_url != null) {
    //                         htmlStr += "<div class='post'><h2 class='post-title'><a href='/feeds/show/" + array[i].id + "'>" + array[i].name + "</a></h2><div class='post-meta'><span><i class='fa fa-clock-o'></i>" + jQuery.timeago(array[i].created_at) + " by <a href='#'>" + array[i].username + "</a></span><span><a href='blog-post.html#comments'><i class='fa fa-comment-o'></i> 98 comments</a></span></div><div class='post-thumbnail'><iframe width='853' height='480' src='" + array[i].video_url + "'frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe></div><p>" + array[i].description + "</p></div>";
    //                     }
    //                     if (array[i].image_path != null) {
    //                         htmlStr += "<div class='post'><h2 class='post-title'><a href='/feeds/show/" + array[i].id + "'>" + array[i].name + "</a></h2><div class='post-meta'><span><i class='fa fa-clock-o'></i>" + jQuery.timeago(array[i].created_at) + " by <a href='#'>" + array[i].username + "</a></span><span><a href='blog-post.html#comments'><i class='fa fa-comment-o'></i> 98 comments</a></span></div><div class='post-thumbnail'><img src='/storage/" + array[i].image_path + "' alt='Uncharted The Lost Legacy First Gameplay Details Revealed'></div><p>" + array[i].description + "</p></div>";
    //                     }
    //                 }
    //             } else {
    //                 htmlStr = "<div>No feeds to display, <a href='/feeds/create'>make one!</a></div>";
    //             }
    //             // $("#post-container").remove();
    //             document.getElementById('feed-container').innerHTML = htmlStr;
    //         }
    //     });
    // }



</script>
@endsection
