@extends('user.layouts.master')

@section ('content')

<?php $title = 'user-games'; ?>
<!-- main -->
<section class="hero hero-profile"
         style="background-image: url('/img/bg/wp2548263.jpg');">
    <div class="overlay" style="opacity: 0.4;"></div>
    <div class="container">
        <div class="hero-block">
            <h5>{{ $user->username }}</h5>
        </div>
    </div>
</section>

<section class="toolbar toolbar-profile" data-fixed="true">
    <div class="container">
        <div class="profile-avatar">
            <a><img @if ($user->avatar == 1) src="/img/user/avatar.png" @else src="/storage/{{ $user->avatar }}" @endif></a>
            <div class="sticky">
                <a><img @if ($user->avatar == 1) src="/img/user/avatar.png" @else src="/storage/{{ $user->avatar }}" @endif></a>
                <div class="profile-info">
                    <h5>{{ $user->username }}</h5>
                    <span>@{{ $user->username }}</span>
                </div>
            </div>
        </div>

        <ul class="toolbar-nav hidden-md-down">
            <li><a href="/feeds/user/{{ $user->id }}">All Feeds</a></li>
            <li @if ($title == 'user-games') class="active" @endif><a href="/feeds/user/games/{{ $user->id }}">All Games</a></li>
        </ul>
    </div>
</section>

<section class="p-y-30">
    <div class="container">
        <div class="row" style="margin-top: 50px;">
            <div class="col-lg-12">
                <div id="games" class="row">
                    @foreach ($games as $game)
                    <div class="col-lg-12">
                        <label class="custom-control custom-checkbox custom-checkbox-danger m-r-10">
                            <input type="checkbox" class="custom-control-input" name="favourite-games[]" value="{{ $game }}" checked disabled>
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description">{{ $game }}</span>
                        </label>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /main -->

@endsection

@section('page-scripts')
@endsection
