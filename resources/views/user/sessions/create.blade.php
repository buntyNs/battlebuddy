@extends('user.layouts.master')

@section('content')

<!-- main -->
<section class="bg-image bg-image-sm" style="background-image: url('img/bg/bg-login.png');">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-8 col-md-4 mx-auto">
                @if ($flash = session('success'))  
                    <div class="alert alert-primary text-center" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ $flash }}
                    </div>
                @endif
                @include('user.layouts.errors')
                <div class="card m-b-0">
                    <div class="card-header">
                        <h4 class="card-title"><i class="fa fa-sign-in"></i> Login to your account</h4>
                    </div>
                    <div class="card-block">
                        <form action="/login" method="POST">
                            @csrf
                            <div class="form-group input-icon-left m-b-10">
                                <i class="fa fa-user"></i>
                                <input type="email" id="email" class="form-control form-control-secondary" name="email" value="{{ old('email') }}" placeholder="Username">
                            </div>
                            <div class="form-group input-icon-left m-b-15">
                                <i class="fa fa-lock"></i>
                                <input type="password" id="password" class="form-control form-control-secondary" name="password" placeholder="Password">
                            </div>
                            <label class="custom-control custom-checkbox custom-checkbox-primary">
                                <input type="checkbox" class="custom-control-input" name="remember-me" value="{{ old('remember-me') }}">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Remember me</span>
                            </label>
                            <button type="submit" class="btn btn-danger btn-block m-t-10">Login <i
                                    class="fa fa-sign-in"></i></button>
                            <div class="divider">
                                <span>Don't have an account?</span>
                            </div>
                            <a class="btn btn-danger btn-block" href="/register" role="button">Register</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /main -->

@endsection
