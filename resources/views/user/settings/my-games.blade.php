@extends('user.layouts.master')

@section ('content')

<?php $title = 'profile-games'; ?>
<!-- main -->
<section class="hero hero-profile"
         style="background-image: url('/img/bg/wp2548263.jpg');">
    <div class="overlay" style="opacity: 0.4;"></div>
    <div class="container">
        <div class="hero-block">
            <h5>{{ Auth::user()->username }}</h5>
        </div>
    </div>
</section>

<section class="toolbar toolbar-profile" data-fixed="true">
    <div class="container">
        <div class="profile-avatar">
            <a href="#"><img @if (Auth::user()->avatar == 1) src="/img/user/avatar.png" @else src="/storage/{{ Auth::user()->avatar }}" @endif alt=""></a>
            <div class="sticky">
                <a href="#"><img @if (Auth::user()->avatar == 1) src="/img/user/avatar.png" @else src="/storage/{{ Auth::user()->avatar }}" @endif alt=""></a>
                <div class="profile-info">
                    <h5>{{ Auth::user()->username }}</h5>
                    <span>@{{ Auth::user()->username }}</span>
                </div>
            </div>
        </div>

        <ul class="toolbar-nav hidden-md-down">
            <li><a href="/user/profile/my-feeds">All My Feeds</a></li>
            <li><a href="/user/profile/change-email">Change Email Address</a></li>
            <li><a href="/user/profile/change-account-settings">Change Account Details</a></li>
            <li @if ($title == 'profile-games') class="active" @endif><a href="/user/profile/my-games">My Games</a></li>
            <li><a href="/user/profile/delete-my-account">Delete My Account</a></li>
        </ul>
    </div>
</section>

<section class="p-y-30">
    <div class="container">
        <div class="row" style="margin-top: 50px;">

                <div class="col-lg-12">
                    @if ($flash = session('success'))  
                        <div class="alert alert-success text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ $flash }}
                        </div>
                    @endif
                    @if ($flash = session('failed'))  
                        <div class="alert alert-danger text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ $flash }}
                        </div>
                    @endif
                    @include('user.layouts.errors')
                <div class="post post-card post-profile">
                    <div class="post-header">
                        <div>
                            <h2 class="post-title" style="margin-bottom: 30px">
                                All My Games
                            </h2>
                            <p class="text-primary">Add new games or disable games you don't play anymore</p>

                            <form action="/user/profile/my-games" method="POST">
                                @csrf
                                <div class="form-group">
                                    <div class="form-group input-icon-left m-b-10">
                                        <div id="games" class="row">
                                            @foreach ($games as $game)
                                            <div class="col-lg-3">
                                                <label class="custom-control custom-checkbox custom-checkbox-danger m-r-10">
                                                    <input type="checkbox" class="custom-control-input" name="favourite-games[]" value="{{ $game }}" checked>
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">{{ $game }}</span>
                                                </label>
                                            </div>
                                            @endforeach
                                        </div>
            
                                        <div class="row col-lg-12">
                                            <div class="col-lg-4">
                                                <div class="form-group input-icon-left m-b-10" style="margin-top: 10px;">
                                                    <i class="fa fa-gamepad"></i>
                                                    <input type="text" id="game" class="form-control form-control-secondary" name="game" placeholder="Type the game name">
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="form-group input-icon-left m-b-10">
                                                    <button type="button" id="addGame" class="btn btn-danger m-t-10 btn-block" onclick="javascript:newGame();">Add</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-primary" style="margin-top: 10px">Change</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
               
            </div>
        </div>
    </div>
</section>
<!-- /main -->

@endsection

@section('page-scripts')
<script>
        function newGame() {
            var game = $("#game").val();
            
            if (game == "" || game == null) {
                return false;
            };

            game = game.trim();
            // game = game.charAt(0).toUpperCase() + game.substr(1).toLowerCase();
            game = game.toLowerCase();
            // aGame = game.toLowerCase();
            $("#game").val('');
            var html = '<div class="col-lg-3"><label class="custom-control custom-checkbox custom-checkbox-danger m-r-10"><input type="checkbox" class="custom-control-input" name="favourite-games[]" value="' + game + '" checked><span class="custom-control-indicator"></span><span class="custom-control-description">' + game + '</span></label></div>';
            $("#games").append(html);
        }
     </script>
@endsection
