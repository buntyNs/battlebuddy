@extends('user.layouts.master')

@section ('content')

<?php $title = 'profile-account'; ?>
<!-- main -->
<section class="hero hero-profile"
         style="background-image: url('/img/bg/wp2548263.jpg');">
    <div class="overlay" style="opacity: 0.4;"></div>
    <div class="container">
        <div class="hero-block">
            <h5>{{ Auth::user()->username }}</h5>
        </div>
    </div>
</section>

<section class="toolbar toolbar-profile" data-fixed="true">
    <div class="container">
        <div class="profile-avatar">
            <a href="#"><img @if (Auth::user()->avatar == 1) src="/img/user/avatar.png" @else src="/storage/{{ Auth::user()->avatar }}" @endif alt=""></a>
            <div class="sticky">
                <a href="#"><img @if (Auth::user()->avatar == 1) src="/img/user/avatar.png" @else src="/storage/{{ Auth::user()->avatar }}" @endif alt=""></a>
                <div class="profile-info">
                    <h5>{{ Auth::user()->username }}</h5>
                    <span>@{{ Auth::user()->username }}</span>
                </div>
            </div>
        </div>

        <ul class="toolbar-nav hidden-md-down">
            <li><a href="/user/profile/my-feeds">All My Feeds</a></li>
            <li><a href="/user/profile/change-email">Change Email Address</a></li>
            <li @if ($title == 'profile-account') class="active" @endif><a href="/user/profile/change-account-settings">Change Account Details</a></li>
            <li><a href="/user/profile/my-games">My Games</a></li>
            <li><a href="/user/profile/delete-my-account">Delete My Account</a></li>
        </ul>
    </div>
</section>

<section class="p-y-30">
    <div class="container">
        <div class="row" style="margin-top: 50px;">

                <div class="col-lg-12">
                    @if ($flash = session('success'))  
                        <div class="alert alert-success text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ $flash }}
                        </div>
                    @endif
                    @if ($flash = session('failed'))  
                        <div class="alert alert-danger text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ $flash }}
                        </div>
                    @endif
                    @include('user.layouts.errors')
                <div class="post post-card post-profile">
                    <div class="post-header">
                        <div>
                            <form action="/user/profile/change-account-settings" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div id="accordion-collapsed" class="accordion accordion-collapsed accordion-icon" role="tablist">
                                    <div class="card">
                                      <div class="card-header" role="tab" id="headingOne">
                                        <h5>
                                          <a data-toggle="collapse" href="#collapsedOne" aria-expanded="true" aria-controls="collapsedOne">
                                          Change Username
                                        </a>
                                        </h5>
                                      </div>
                                      <div id="collapsedOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion-collapsed">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="old-username" value="{{ Auth::user()->username }}" placeholder="Old username" disabled>
                                                <input type="text" class="form-control" id="username" name="username" value="{{ old('username') }}" placeholder="New username">
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="card">
                                      <div class="card-header" role="tab" id="headingTwo">
                                        <h5>
                                          <a class="collapsed" data-toggle="collapse" href="#collapsedTwo" aria-expanded="false" aria-controls="collapsedTwo">
                                            Change Password
                                        </a>
                                        </h5>
                                      </div>
                                      <div id="collapsedTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion-collapsed">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <input type="password" class="form-control" id="old-password" name="old-password" value="{{ old('old-password') }}" placeholder="Old password">
                                                <input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}" placeholder="New password">
                                                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" value="{{ old('password_confirmation') }}" placeholder="Confirm password">
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="card">
                                      <div class="card-header" role="tab" id="headingThree">
                                        <h5>
                                          <a class="collapsed" data-toggle="collapse" href="#collapsedThree" aria-expanded="false" aria-controls="collapsedThree">
                                          Change Profile Picture
                                        </a>
                                        </h5>
                                      </div>
                                      <div id="collapsedThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion-collapsed">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <input type="file" name="profile-image" accept="image/x-png,image/gif,image/jpeg" onchange="readURL(this);" style="padding: 8px 0px 8px 0px">
                                                <img id="profile-picture" src="https://via.placeholder.com/150" alt="Profile Picture" style="max-width:180px;" />
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <button type="submit" class="btn btn-primary" style="margin-top: 10px">Change</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /main -->

@endsection

@section('page-scripts')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#profile-picture')
                .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection
