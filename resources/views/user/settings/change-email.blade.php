@extends('user.layouts.master')

@section ('content')

<?php $title = 'profile-email'; ?>
<!-- main -->
<section class="hero hero-profile"
         style="background-image: url('/img/bg/wp2548263.jpg');">
    <div class="overlay" style="opacity: 0.4;"></div>
    <div class="container">
        <div class="hero-block">
            <h5>{{ Auth::user()->username }}</h5>
        </div>
    </div>
</section>

<section class="toolbar toolbar-profile" data-fixed="true">
    <div class="container">
        <div class="profile-avatar">
            <a href="#"><img @if (Auth::user()->avatar == 1) src="/img/user/avatar.png" @else src="/storage/{{ Auth::user()->avatar }}" @endif alt=""></a>
            <div class="sticky">
                <a href="#"><img @if (Auth::user()->avatar == 1) src="/img/user/avatar.png" @else src="/storage/{{ Auth::user()->avatar }}" @endif alt=""></a>
                <div class="profile-info">
                    <h5>{{ Auth::user()->username }}</h5>
                    <span>@{{ Auth::user()->username }}</span>
                </div>
            </div>
        </div>

        <ul class="toolbar-nav hidden-md-down">
            <li><a href="/user/profile/my-feeds">All My Feeds</a></li>
            <li @if ($title == 'profile-email') class="active" @endif><a href="/user/profile/change-email">Change Email Address</a></li>
            <li><a href="/user/profile/change-account-settings">Change Account Details</a></li>
            <li><a href="/user/profile/my-games">My Games</a></li>
            <li><a href="/user/profile/delete-my-account">Delete My Account</a></li>
        </ul>
    </div>
</section>

<section class="p-y-30">
    <div class="container">
        <div class="row" style="margin-top: 50px;">

                <div class="col-lg-12">
                    @if ($flash = session('success'))  
                        <div class="alert alert-success text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ $flash }}
                        </div>
                    @endif
                    @if ($flash = session('failed'))  
                        <div class="alert alert-danger text-center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ $flash }}
                        </div>
                    @endif
                    @include('user.layouts.errors')
                <div class="post post-card post-profile">
                    <div class="post-header">
                        <div>
                            <h2 class="post-title" style="margin-bottom: 30px">
                                Change Email Address
                            </h2>
                            <form action="/user/profile/change-email" method="POST">
                                @csrf
                                <div class="form-group">
                                    {{-- <label for="exampleInputEmail1">Email address</label> --}}
                                    <input type="email" class="form-control" id="old-email" name="old-email" value="{{ Auth::user()->email }}" placeholder="Old email" disabled>
                                    <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" placeholder="New email">
                                    <input type="email" class="form-control" id="email_confirmation" name="email_confirmation" value="{{ old('email_confirmation') }}" placeholder="Confirm email">
                                    <button type="submit" class="btn btn-primary" style="margin-top: 10px">Change</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
               
            </div>
        </div>
    </div>
</section>
<!-- /main -->

@endsection

@section('page-scripts')
@endsection
