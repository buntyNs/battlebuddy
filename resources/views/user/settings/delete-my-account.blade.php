@extends('user.layouts.master')

@section ('content')

<?php $title = 'profile-delete'; ?>
<!-- main -->
<section class="hero hero-profile"
         style="background-image: url('/img/bg/wp2548263.jpg');">
    <div class="overlay" style="opacity: 0.4;"></div>
    <div class="container">
        <div class="hero-block">
            <h5>{{ Auth::user()->username }}</h5>
        </div>
    </div>
</section>

<section class="toolbar toolbar-profile" data-fixed="true">
    <div class="container">
        <div class="profile-avatar">
            <a href="#"><img @if (Auth::user()->avatar == 1) src="/img/user/avatar.png" @else src="/storage/{{ Auth::user()->avatar }}" @endif alt=""></a>
            <div class="sticky">
                <a href="#"><img @if (Auth::user()->avatar == 1) src="/img/user/avatar.png" @else src="/storage/{{ Auth::user()->avatar }}" @endif alt=""></a>
                <div class="profile-info">
                    <h5>{{ Auth::user()->username }}</h5>
                    <span>@{{ Auth::user()->username }}</span>
                </div>
            </div>
        </div>

        <ul class="toolbar-nav hidden-md-down">
            <li><a href="/user/profile/my-feeds">All My Feeds</a></li>
            <li><a href="/user/profile/change-email">Change Email Address</a></li>
            <li><a href="/user/profile/change-account-settings">Change Account Details</a></li>
            <li><a href="/user/profile/my-games">My Games</a></li>
            <li @if ($title == 'profile-delete') class="active" @endif><a href="/user/profile/delete-my-account">Delete My Account</a></li>
        </ul>
    </div>
</section>

<section class="p-y-30">
    <div class="container">
        <div class="row" style="margin-top: 50px;">
            <div class="col-lg-12">
                <div class="post post-card post-profile">
                    <div class="post-header">
                        <div>
                            <h2 class="post-title" style="margin-bottom: 30px">
                                Delete My Account
                            </h2>
                            <p class="text-warning">This action cannot be undone. The changes make you here is permanent.</p>
                            <a type="button" class="text-white btn btn-danger" role="button" data-toggle="modal" data-target="#modal-danger">Delete Account</a>
                            <div class="modal fade modal-center modal-danger" id="modal-danger">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="ModalLabel">Are you sure want to delete the account</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p class="text-center">Yes, delete all my settings and the account itself!</p>
                                        </div>
                                        <div class="modal-footer">
                                            <a type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</a>
                                            <a href="/user/profile/delete-account" type="button" class="text-white btn btn-danger">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /main -->

@endsection
