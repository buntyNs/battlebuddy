@extends('user.layouts.master')

@section ('content')

<?php $title = 'profile-feeds'; ?>
<!-- main -->
<section class="hero hero-profile"
         style="background-image: url('/img/bg/wp2548263.jpg');">
    <div class="overlay" style="opacity: 0.4;"></div>
    <div class="container">
        <div class="hero-block">
            <h5>{{ Auth::user()->username }}</h5>
        </div>
    </div>
</section>

<section class="toolbar toolbar-profile" data-fixed="true">
    <div class="container">
        <div class="profile-avatar">
            <a><img @if (Auth::user()->avatar == 1) src="/img/user/avatar.png" @else src="/storage/{{ Auth::user()->avatar }}" @endif></a>
            <div class="sticky">
                <a><img @if (Auth::user()->avatar == 1) src="/img/user/avatar.png" @else src="/storage/{{ Auth::user()->avatar }}" @endif></a>
                <div class="profile-info">
                    <h5>{{ Auth::user()->username }}</h5>
                    <span>@{{ Auth::user()->username }}</span>
                </div>
            </div>
        </div>

        <ul class="toolbar-nav hidden-md-down">
            <li @if ($title == 'profile-feeds') class="active" @endif><a href="/user/profile/my-feeds/{{ Auth::user()->id }}">All My Feeds</a></li>
            <li><a href="/user/profile/change-email">Change Email Address</a></li>
            <li><a href="/user/profile/change-account-settings">Change Account Details</a></li>
            <li><a href="/user/profile/my-games">My Games</a></li>
            <li><a href="/user/profile/delete-my-account">Delete My Account</a></li>
        </ul>
    </div>
</section>

<section class="p-y-30">
    <div class="container">
        <div class="row" style="margin-top: 50px;">

                <div class="col-lg-12">
                    <!-- post -->
                @if (count($feeds) < 1)
                <p>Currently there are no feeds to show, <a href="/feeds/create" class="text-primary">create one</a></p>
                @endif
                @foreach ($feeds as $feed)
                <div class="post post-card post-profile">
                    <div class="post-header">
                        <div>
                            <a href="/user/profile/my-feeds">
                                <img @if (Auth::user()->avatar == 1) src="/img/user/avatar.png" @else src="/storage/{{ Auth::user()->avatar }}" @endif>
                            </a>
                        </div>
                        <div>
                            <h2 class="post-title">
                                <a href="/feeds/show/{{ $feed->id }}">{{ $feed->name }}</a>
                            </h2>
                            <div class="post-meta">
                                <span><i class="fa fa-clock-o"></i> {{ $feed->created_at->diffForHumans() }}</span>
                                @if ($feed->like_count != null || $feed->like_count != 0) <span><a><i class="fa fa-heart"></i> {{ $feed->like_count }} likes</a></span> @else <span><a><i class="fa fa-heart-o"></i>0 likes</a></span>@endif
                            </div>
                        </div>
                    </div>
                    <p>{{ $feed->description }}</p>
                    <div class="post-thumbnail">
                            @if (!is_null($feed->image_path))
                            <img src="/storage/{{ $feed->image_path }}" alt="/storage/{{ $feed->name }}">
                            @endif
                            @if (!is_null($feed->video_url))
                            <iframe width="853" height="480" src="{{ $feed->video_url }}" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            @endif
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
<!-- /main -->

@endsection

@section('page-scripts')
@endsection
