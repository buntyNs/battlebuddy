@extends('user.layouts.master')

@section('content')

<!-- main -->
<section class="bg-image bg-image-sm" style="background-image: url('img/bg/bg-login.png');">
    <div class="overlay" style="opacity: 0.3!important;"></div>

    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-8 col-md-4 mx-auto" style="margin-left: 300px!important;">
                @if ($flash = session('failed'))  
                    <div class="alert alert-danger text-center" style="width: 600px" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ $flash }}
                    </div>
                @endif
                @if ($flash = session('failed-duplicate-email'))  
                    <div class="alert alert-warning text-center" style="width: 600px" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        {{ $flash }}
                    </div>
                @endif
                @include('user.layouts.errors')
                <div class="card" style="width: 600px;">
                    <div class="card-header">
                        <h4 class="card-title"><i class="fa fa-user-plus"></i> Register a new account</h4>
                    </div>
                    <div class="card-block">
                        <form class="" action="/register" method="POST">
                            @csrf
                            <div class="form-group input-icon-left m-b-10">
                                <i class="fa fa-user"></i>
                                <input type="text" id="username" class="form-control form-control-secondary" name="username" placeholder="Username" required>
                            </div>

                            <div class="form-group input-icon-left m-b-10">
                                <i class="fa fa-lock"></i>
                                <input type="password" id="password" class="form-control form-control-secondary" name="password" placeholder="Password" required>
                            </div>

                            <div class="form-group input-icon-left m-b-10">
                                <i class="fa fa-unlock"></i>
                                <input type="password" id="password_confirmation" class="form-control form-control-secondary" name="password_confirmation" placeholder="Repeat Password" required>
                            </div>

                            <div class="divider"><span>Personal</span></div>

                            <div class="form-group input-icon-left m-b-10">
                                <i class="fa fa-globe"></i>
                                <select id="region" class="form-control form-control-secondary" name="region">
                                    <option value="europe">Europe</option>
                                    <option value="asia">Asia</option>
                                    <option value="north_america">North America</option>
                                    <option value="south_america">South America</option>
                                    <option value="africa">Africa</option>
                                    <option value="australia">Australia</option>
                                </select>
                            </div>                           

                            <div class="form-group input-icon-left m-b-10">
                                <i class="fa fa-envelope"></i>
                                <input type="email" id="email" class="form-control form-control-secondary" name="email" placeholder="Email Address" required>
                            </div>

                            <div class="form-group input-icon-left m-b-10">
                                <!--<label for="date">Date of Birth:</label>-->
                                <i class="fa fa-calendar"></i>
                                <input type="date" id="dob" class="form-control form-control-secondary" id="dob" name="dob" placeholder="Date of birth" required>
                            </div>

                            <div class="divider"><span>Select some games you like to play, These will be higlighted in your battlebuddy tab</span></div>

                            <div class="form-group input-icon-left m-b-10">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label class="custom-control custom-checkbox custom-checkbox-danger m-r-10">
                                            <input type="checkbox" class="custom-control-input" name="favourite-games[]" value="apex-legends">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Apex Legends</span>
                                        </label>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="custom-control custom-checkbox custom-checkbox-danger m-r-10">
                                            <input type="checkbox" class="custom-control-input" name="favourite-games[]" value="border-land-2">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Boarder Land 2</span>
                                        </label>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="custom-control custom-checkbox custom-checkbox-danger m-r-10">
                                            <input type="checkbox" class="custom-control-input" name="favourite-games[]" value="rainbow-6">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Rainbow 6</span>
                                        </label>
                                    </div>
                                </div>

                                <div id="games" class="row">
                                    <div class="col-lg-4">
                                        <label class="custom-control custom-checkbox custom-checkbox-danger m-r-10">
                                            <input type="checkbox" class="custom-control-input" name="favourite-games[]" value="tom-clancy-devision-2">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Tom Clancy Devision 2</span>
                                        </label>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="custom-control custom-checkbox custom-checkbox-danger m-r-10">
                                            <input type="checkbox" class="custom-control-input" name="favourite-games[]" value="call-of-duty-ww2">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Call of Duty WW2</span>
                                        </label>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="custom-control custom-checkbox custom-checkbox-danger m-r-10">
                                            <input type="checkbox" class="custom-control-input" name="favourite-games[]" value="battlefield-2">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">Battlefield 2</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-10">
                                        <div class="form-group input-icon-left m-b-10" style="margin-top: 10px;">
                                            <i class="fa fa-gamepad"></i>
                                            <input type="text" id="game" class="form-control form-control-secondary" name="game" placeholder="Type the game name">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group input-icon-left m-b-10">
                                            <button id="addGame" class="btn btn-danger m-t-10 btn-block" onclick="javascript:newGame();">Add</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="divider"><span>I am not a robot</span></div>
                            <div class="g-recaptcha-outer">
                                <script src='https://www.google.com/recaptcha/api.js'></script>
                                <div class="g-recaptcha" data-sitekey="6LeBwhwUAAAAAG1RDj-rS2Wu4WYNoV021q0z-LNY"></div>
                            </div>

                            <div class="divider"><span>Terms of Service</span></div>

                            <label class="custom-control custom-checkbox custom-checkbox-danger">
                                <input type="checkbox" id="terms-and-conditions" class="custom-control-input" name="terms-and-conditions" required>
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">Accept <a href="#" data-toggle="modal" data-target="#terms">terms of service</a></span>
                            </label>

                            <button type="submit" class="btn btn-danger m-t-10 btn-block">Complete Registration</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="terms">
        <div class="modal-dialog modal-top" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fa fa-file-text-o"></i> Terms of Service</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h6>YOUR AGREEMENT</h6>
                    <p>By using this Site, you agree to be bound by, and to comply with, these Terms and Conditions. If you do not agree to these Terms and Conditions, please do not use this site.
                        
                        PLEASE NOTE: We reserve the right, at our sole discretion, to change, modify or otherwise alter these Terms and Conditions at any time. Unless otherwise indicated, amendments will become effective immediately. Please review these Terms and Conditions periodically. Your continued use of the Site following the posting of changes and/or modifications will constitute your acceptance of the revised Terms and Conditions and the reasonableness of these standards for notice of changes. For your information, this page was last updated as of the date at the top of these terms and conditions.</p>
                        
                    <h6>PRIVACY</h6>
                    <p>Please review our Privacy Policy, which also governs your visit to this Site, to understand our practices.</p>

                    <p>Ut viverra urna non orci interdum, in viverra urna pretium. Suspendisse potenti. Mauris et massa
                        a enim lobortis facilisis. In hac habitasse platea dictumst. Ut varius erat vulputate libero
                        sagittis, vitae feugiat nibh malesuada. Sed vel lacinia
                        urna. Curabitur eget dui nisi.</p>
                    <p>Vivamus orci felis, varius tempor lacus eu, scelerisque bibendum nunc. Vestibulum rutrum, enim
                        quis maximus pretium, nisi est condimentum magna, venenatis dignissim magna nulla quis felis.
                        Quisque id tellus nec mauris sagittis mattis non et
                        quam. Etiam posuere, tellus sed tincidunt egestas, tortor orci interdum risus, nec egestas dolor
                        tortor non turpis. Curabitur in tellus laoreet, congue eros a, bibendum tortor. Nulla in
                        facilisis libero, sit amet sagittis tellus. Aliquam
                        nec pulvinar velit, mattis pharetra urna. Donec et tincidunt libero. Duis at nisi in neque
                        vulputate tempus. Curabitur et lobortis lacus. In sagittis egestas lorem, nec bibendum lacus
                        maximus sed.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="deny()">Close</button>
                    <button type="button" id="accept" class="btn btn-danger" data-dismiss="modal" onclick="accept()">Accept</button>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /main -->

@endsection

@section('page-scripts')
    <script>
        function newGame() {
            var game = $("#game").val();
            
            if (game == "" || game == null) {
                return false;
            };

            game = game.trim();
            game = game.charAt(0).toUpperCase() + game.substr(1).toLowerCase();
            aGame = game.toLowerCase();
            $("#game").val('');
            var html = '<div class="col-lg-4"><label class="custom-control custom-checkbox custom-checkbox-danger m-r-10"><input type="checkbox" class="custom-control-input" name="favourite-games[]" value="' + aGame + '" checked><span class="custom-control-indicator"></span><span class="custom-control-description">' + game + '</span></label></div>';
            $("#games").append(html);
        }

        function accept() {
            $('input[id=terms-and-conditions]').attr('checked', true);
        }

        function deny() {
            $('input[id=terms-and-conditions]').attr('checked', false);
        }
     </script>
@endsection
