<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('parent_comment_id_reply_id')->nullable();
            $table->integer('user_id');
            $table->integer('feed_id');
            $table->text('message');
            $table->integer('like_count')->nullable();
            $table->integer('dislike_count')->nullable();
            $table->boolean('is_spam')->nullable();
            $table->boolean('have_reply')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
