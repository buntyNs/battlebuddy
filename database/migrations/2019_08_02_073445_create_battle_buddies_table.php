<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBattleBuddiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('battle_buddies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            // $table->unsignedBigInteger('game_id');
            // $table->foreign('game_id')->references('id')->on('games');
            $table->string('username');
            $table->string('game_name');
            $table->string('gamer_id');
            $table->string('platform');
            $table->timeTz('online_time')->nullable();
            $table->boolean('voice_chat');
            $table->string('region');
            $table->string('language');
            $table->text('message');
            $table->boolean('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('battle_buddies');
    }
}
