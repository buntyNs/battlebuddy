<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feeds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->string('name');
            $table->string('gamer_id');
            $table->string('tags');
            $table->string('image_path')->nullable();
            $table->text('video_url')->nullable();
            $table->text('description');
            $table->boolean('add_to_battlebuddy');
            $table->boolean('is_owner');
            $table->integer('like_count')->nullable();
            $table->integer('dislike_count')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feeds');
    }
}
