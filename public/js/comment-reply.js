var APP_URL = "http://157.230.122.77";
function sendComment() {
    var data = {
        "message":$("#message").val(),
        "feed_id":$("#feed-id").val(),
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: APP_URL+"/feeds/comments/create",
        method: "POST",
        data: data,
            success: function( response ) {
            $("#comment-container").append("<li><div id='com" + response.commentId + "' class='comment'><div class='comment-avatar'><a href='/feeds/user/" + response.userId + "'><img src=" + response.avatar + "></a></div><div class='comment-post'><div class='comment-header'><div class='comment-author'><h5><a href='/feeds/user/" + response.userId + "'>" + response.username + "</a><span class='badge badge-outline-dark'>" + response.role + "</span></h5></div><div class='comment-action'><div class='comment-action'><div class='dropdown float-right'><a href='#' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='fa fa-chevron-down'></i></a><div class='dropdown-menu dropdown-menu-right'><a class='dropdown-item' href='#'>Mark as spam</a></div></div></div></div></div><p>" + response.data.message + "</p><div class='comment-footer'><ul><li><a id='comment-id' value='" + response.commentId + "' commentId='" + response.commentId + "' class='like' onclick='like(this)'><i class='fa fa-heart-o'></i> Like</a></li><li><a class='reply' superComment='" + response.commentId + "' clicked='no' status='ajaxComment' id='" + response.commentId + "' commentId='com" + response.commentId +"' onclick='reply(this)'><i class='icon-reply'></i> Reply</a></li><li><a><i class='fa fa-clock-o'></i> " + response.date + "</a></li><li id='deleteComment" + response.commentId + "'><a onclick='deleteComment(this)' id='" + response.commentId + "'><span class='text-danger'><i class='fa fa-trash'></i> Delete</span></a></li></ul></div></div></div><ul id='replyContainer" + response.commentId + "'></ul></li>");
            document.getElementById("comment").reset();
        }
    });
}

function sendReply(commentId) {
    if (commentId.hasAttribute('superCommentId')) {
        var comment_id = commentId.getAttribute('superCommentId');
    } else {
        var comment_id = commentId.getAttribute('comId');
    }

    var data = {
        "feed_id":$("#feed-id").val(),
        "reply":$("#reply").val(),
        "comment_id":comment_id,
        'nested':commentId.getAttribute('i')
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: APP_URL+"/feeds/comments/reply/store",
        method: "POST",
        data: data,
            success: function( response ) {

            var parentCommentId = "replyContainer"+comment_id;

            $("#"+parentCommentId).append("<li id='nestedReply" + response.commentId + "'><div id='com" + response.commentId + "' class='comment mb-3'><div class='comment-avatar'><a href='/feeds/user/" + response.userId + "'><img src=" + response.avatar + "></a></div><div class='comment-post'><div class='comment-header'><div class='comment-author'><h5><a href='/feeds/user/" + response.userId + "'>" + response.username + "</a><span class='badge badge-outline-dark'>" + response.role + "</span></h5></div><div class='comment-action'><div class='comment-action'><div class='dropdown float-right'><a href='#' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='fa fa-chevron-down'></i></a><div class='dropdown-menu dropdown-menu-right'><a class='dropdown-item' href='#'>Mark as spam</a></div></div></div></div></div><p>" + response.data.message + "</p><div class='comment-footer'><ul><li><a commentId='" + response.commentId + "' class='like' onclick='like(this)'><i class='fa fa-heart-o'></i> Like</a></li><li><a class='reply' superComment='" + comment_id + "' clicked='no' id='" + response.commentId + "' commentId='com" + response.commentId + "' onclick='reply(this)'><i class='icon-reply'></i> Reply</a></li><li><a><i class='fa fa-clock-o'></i> " + response.date + "</a></li><li id='deleteComment" + response.commentId + "'><a onclick='deleteComment(this)' id='" + response.commentId + "'><span class='text-danger'><i class='fa fa-trash'></i> Delete</span></a></li></ul></div></div></div></li>");
            document.getElementById("replyFormcom"+commentId.getAttribute('comId')).reset();
            document.getElementById("replyFormcom"+commentId.getAttribute('comId')).remove();

            var parentComment = "#com" + commentId.getAttribute('comid');
            parentComment = parentComment + " .reply";
            var parentComment = document.querySelector(parentComment);
            parentComment.setAttribute('clicked','no');
        }
    });
}

$(".reply").on("click", function() {
    var replyCommentId = this.getAttribute("commentId");
    if (this.getAttribute('clicked') == "no") {
        var id = this.getAttribute("id");
        var str ="#"+replyCommentId;
        $(str).after("<form id='replyForm" + replyCommentId + "'><div class='input-group'><input type='text' id='reply' class='form-control' style='margin-top:5px'><input type='hidden' id='commentID' value=''><span class='input-group-btn'><button comId = "+id+" type='button' style='margin-top: 5px' onclick='sendReply(this)' class='sendReply btn btn-primary btn-shadow'>Submit</button></span></div></form>");
        this.setAttribute('clicked','yes');
    } else {
        var str = "replyForm"+replyCommentId;
        document.getElementById(str).remove();
        this.setAttribute('clicked','no');
    }
});

function reply(event) {
    var replyCommentId = event.getAttribute("commentId");
    var replySuperCommentId = event.getAttribute("superComment");
    var ajaxComment = event.getAttribute("status");
    if (event.getAttribute('clicked') == "no") {
        var id = event.getAttribute("id");
        var str ="#"+replyCommentId;
        if (ajaxComment == 'ajaxComment') {
            $(str).after("<form id='replyForm" + replyCommentId + "'><div class='input-group'><input type='text' id='reply' class='form-control' style='margin-top:5px'><input type='hidden' id='commentID' value=''><span class='input-group-btn'><button comId = "+id+" superCommentId='" + replySuperCommentId + "' type='button' style='margin-top: 5px' onclick='sendReply(this)' class='sendReply btn btn-primary btn-shadow'>Submit</button></span></div></form>");
        } else {
            $(str).after("<form id='replyForm" + replyCommentId + "'><div class='input-group'><input type='text' id='reply' class='form-control' style='margin-top:5px'><input type='hidden' id='commentID' value=''><span class='input-group-btn'><button comId = "+id+" superCommentId='" + replySuperCommentId + "' type='button' style='margin-top: 5px' onclick='sendReply(this)' class='sendReply btn btn-primary btn-shadow' i='1'>Submit</button></span></div></form>");
        }
        event.setAttribute('clicked','yes');
    } else {
        var str = "replyForm"+replyCommentId;
        document.getElementById(str).remove();
        event.setAttribute('clicked','no');
    }
}
