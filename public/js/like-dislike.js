var APP_URL = "http://157.230.122.77";
$(".like").on("click", function(event) {
    event.preventDefault();
    if ($(event.target.firstChild).hasClass( "fa fa-heart" ) && $(event.target.firstChild).hasClass( "text-primary" )) {
        var data = {
            "comment-id":this.getAttribute('commentId'),
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: APP_URL+"/feeds/comments/unlike",
            method: "GET",
            data: data,
            success: function( response ) {
              $(event.target).removeClass( "text-primary" );
              $(event.target.firstChild).removeClass( "fa fa-heart text-primary" ).addClass( "fa fa-heart-o" );
            }
        });
    } else {
        var data = {
            "comment-id":this.getAttribute('commentId'),
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: APP_URL+"/feeds/comments/like",
            method: "GET",
            data: data,
            success: function( response ) {
                $(event.target).addClass( "text-primary" );
                $(event.target.firstChild).removeClass( "fa fa-heart-o" ).addClass( "text-primary fa fa-heart" );
            }
        });
    }
});

function like(event) {
    if ($(event.firstChild).hasClass( "fa fa-heart" ) && $(event.firstChild).hasClass( "text-primary" )) {
        var data = {
            "comment-id":event.getAttribute('commentId'),
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: APP_URL+"/feeds/comments/unlike",
            method: "GET",
            data: data,
            success: function( response ) {
              $(event).removeClass( "text-primary" );
              $(event.firstChild).removeClass( "fa fa-heart text-primary" ).addClass( "fa fa-heart-o" );
            }
        });
    } else {
        var data = {
            "comment-id":event.getAttribute('commentId'),
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: APP_URL+"/feeds/comments/like",
            method: "GET",
            data: data,
            success: function( response ) {
                $(event).addClass( "text-primary" );
                $(event.firstChild).removeClass( "fa fa-heart-o" ).addClass( "text-primary fa fa-heart" );
            }
        });
    }
}
