var APP_URL = "http://157.230.122.77";
$(".spam").on("click", function (event) {
    var spamId = this.getAttribute('spamId');
    var data = {
        "comment-id": spamId
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url:APP_URL+"/feeds/comments/spam",
        method: "GET",
        data: data,
            success: function( response ) {
                if (response.isSpam == 0) {
                    $("#spam" + response.commentId).html("Mark as spam");
                }
                if (response.isSpam == 1) {
                    $("#spam" + response.commentId).html("Mark as not spam");
                }
        }
    });
});

function spam(event) {
    var spamId = event.getAttribute('spamId');
    var data = {
        "comment-id": spamId
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: 'http://127.0.0.1:8000/feeds/comments/spam',
        method: "GET",
        data: data,
            success: function( response ) {
                if (response.isSpam == 0) {
                    $("#spam" + response.commentId).html("Mark as spam");
                }
                if (response.isSpam == 1) {
                    $("#spam" + response.commentId).html("Mark as not spam");
                }
        }
    });
}
