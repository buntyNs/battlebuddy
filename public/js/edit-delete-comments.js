var APP_URL = "http://157.230.122.77";
function deleteComment(event) {
    var data = {
        'id':event.id
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url:APP_URL+"/feeds/comments/delete",
        method : "GET",
        data : data,
        success : function (response) {
            var commentsDeleted = response.id.length;
            if (commentsDeleted > 0) {
                for (var i=0; i<commentsDeleted; i++) {
                    $("#com"+ response.id[i]).remove();
                }
            } else {
                var message = response;

                $("#deleteComment"+event.id).after("<span class='text-danger'>Deletion " + message + "</span>");
            }
        }
    });
}

// function editComment(event) {
//     var body = document.getElementById("commentBody" + event.id).textContent;
//     document.getElementById("commentBody" + event.id).remove();
//     document.getElementById("commentFooter" + event.id).remove();
//     $("#commentHeader" + event.id).after("<form id='replyForm12'><div class='input-group'><input type='text' id='reply1212' class='form-control' style='margin-top:5px'><input type='hidden' id='commentID' value=''><span class='input-group-btn'><button comId='12' type='button' style='margin-top: 5px' onclick='sendReply(this)' class='sendReply btn btn-primary btn-shadow'>Submit</button></span></div></form>");
//     document.getElementById("reply1212").innerHTML = body;
// }
