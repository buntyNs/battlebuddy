var APP_URL = "http://157.230.122.77";
$(".filter").on("click", function (event) {
    event.preventDefault();
    var data = {
        'filter':this.getAttribute('filter'),
        'feed-id':this.getAttribute('feedId')
    };

    var filterName = this.innerHTML;
    var filter = document.getElementById("comment-filter").innerHTML;
    var content = filter.substr(0, 10);
    var otherContent = filter.slice(-27);
    content = content + filterName + otherContent;
    document.getElementById("comment-filter").innerHTML = content;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: APP_URL+'/feeds/comments/filter',
        method: "GET",
        data: data,
        success: function( response ) {
            var array = response.data;
            var htmlContent = "";
            for (let i = 0; i < array.length; i++) {
                var status = array[i].status;
                var spam = array[i].is_spam;
                if (status == 1) {
                    var textLike = "'text-primary like'";
                    var faHeart = "'text-primary fa fa-heart'";
                } else {
                    var textLike = "like";
                    var faHeart = "'fa fa-heart-o'";
                }

                if (spam == 1) {
                    spam = "Mark as not spam";
                } else {
                    spam = "Mark as spam";
                }

                if (array[i].avatar == 1 || array[i].avatar == "1") { var avatar = "/img/user/avatar.png"; } else { var avatar = "/storage/" + array[i].avatar; }

                if (array[i].have_reply == 1) {
                    var loadReplyButton = "<button class='btn btn-primary text-center m-t-15 btn-block text-white load-replies' onclick='loadReplies(this)' id='loadreplies" + array[i].comment_id + "' comment-id='" + array[i].comment_id + "'><i class='fa fa-spinner fa-pulse m-r-5'></i> Load replies</button>";
                } else {
                    var loadReplyButton = "";
                }

                htmlContent += "<li><div id='com" + array[i].comment_id + "' class='comment'><div class='comment-avatar'><a href='/user/profile/my-feeds'><img src='" + avatar + "'></a></div><div class='comment-post'><div class='comment-header'><div class='comment-author'><h5><a href='/user/profile/my-feeds'>" + array[i].username + "</a></h5></div><div class='comment-action'><div class='comment-action'><div class='dropdown float-right'><a href='#' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='fa fa-chevron-down'></i></a><div class='dropdown-menu dropdown-menu-right'><a id='spam" + array[i].comment_id + "' spamId='" + array[i].comment_id + "' class='spam dropdown-item' onclick='spam(this)'>" + spam + "</a></div></div></div></div></div><p>" + array[i].message + "</p><div class='comment-footer'><ul><li><a id='comment-id' value='" + array[i].comment_id + "' commentId='" + array[i].comment_id + "' class=" + textLike + " onclick='like(this)'><i class=" + faHeart + "></i> Like</a></li><li><a class='reply' id='" + array[i].comment_id + "' commentId='com" + array[i].comment_id +"' onclick='reply(this)'><i class='icon-reply'></i> Reply</a></li><li><a><i class='fa fa-clock-o'></i> " + jQuery.timeago(array[i].created_at) + "</a></li></ul></div></div></div>" + loadReplyButton + "</li><ul id='replyContainer" + array[i].comment_id + "'></ul>";
            }
            // $("#comment-container").remove();
            document.getElementById("comment-container").innerHTML = htmlContent;
        }
    });
});
