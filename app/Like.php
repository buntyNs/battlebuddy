<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $fillable = [
		'type_id',
		'user_id',
		'post_comment_id',
		'status',
	];
}
