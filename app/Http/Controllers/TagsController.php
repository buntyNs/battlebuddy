<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;

class TagsController extends Controller
{
    public function index(Tag $tag)
	{
		$feeds = $tag->feeds;
		return view('user.feeds.index', compact('feeds'));
	}
}
