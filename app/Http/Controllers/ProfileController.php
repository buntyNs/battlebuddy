<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feed;
use App\User;
use App\Comment;
use App\UserDump;
use Auth;
use DB;
use Storage;
use Carbon\Carbon;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function allMyFeeds()
    {
        // $feeds = DB::table('feeds')
        // ->join('users', 'users.id', '=', 'feeds.user_id')
        // ->select('feeds.*', 'users.username', 'users.avatar')
        // ->where('user_id', '=', Auth::user()->id)
        // ->orderBy('created_at', 'desc')
        // ->get();

        $feeds = Feed::all()->where('user_id', '=', Auth::user()->id);
        // $commentCount = Comment::all()->where('user_id', '=', Auth::user()->id)->count();
        // dd($feeds[0]->created_at->diffForHumans());
        return view('user.settings.index', compact('feeds'));
    }

    public function allMyFeed($userId)
    {
        $feeds = Feed::all()->where('user_id', '=', $userId);
        return view('user.settings.index', compact('feeds'));
    }

    public function changeEmail()
    {
        return view('user.settings.change-email');
    }

    public function newEmail(Request $request)
    {
        $this->validate(request(), [
            'email' => 'required|email|confirmed'
        ]);

        DB::table('users')->where('id', '=', Auth::user()->id)->update([
            'email' => request('email')
        ]);
        session()->flash('success', 'Successfully updated your email.');
        return back();
    }

    public function changeAccountSettings()
    {
        return view('user.settings.change-account-details');
    }

    public function storeAccountSettings(Request $request)
    {
        $this->validate(request(), [
            'username' => 'sometimes',
            'old-password' => 'sometimes',
            'password' => 'sometimes|confirmed',
            'profile-image' => 'sometimes|image'
        ]);

        if (request('username')) {
            $username = request('username');
        } else {
            $username = DB::table('users')->select('username')->where('id', '=', Auth::user()->id)->get();
            $username = $username[0]->username;
        }

        if (request('profile-image')) {
            $profilePicture = Storage::disk('public')->put("uploads", request('profile-image'));
        } else {
            $profilePicture = DB::table('users')->select('avatar')->get();
            $profilePicture = $profilePicture[0]->avatar;
        }

        if (request('password')) {
            $password = bcrypt(request('password'));
        } else {
            $password = DB::table('users')->select('password')->get();
            $password = $password[0]->password;
        }

        DB::table('users')->where('id', '=', Auth::user()->id)->update([
            'username' => $username,
            'password' => $password,
            'avatar' => $profilePicture
        ]);
        
        session()->flash('success', 'Successfully updated your account details.');
        return back();
    }

    public function myGames()
    {
        // $games = Auth::user()->favourite_games;
        $games = User::find(Auth::user()->id)->favourite_games;
        $games = explode(',', $games);
        $games = str_replace("-", " ", $games);
        return view('user.settings.my-games', compact('games'));
    }

    public function storeMyGames(Request $request)
    {
        $this->validate(request(), [
            'favourite-games' => 'required|array'
        ]);

        $favouriteGames = implode(',', request('favourite-games'));
        DB::table('users')->where('id', '=', Auth::user()->id)->update([
            'favourite_games' => $favouriteGames
        ]);

        session()->flash('success', 'Successfully updated your favourite games list.');
        return back();
    }

    public function deleteAccount()
    {
        return view('user.settings.delete-my-account');
    }

    public function deleteMyAccount()
    {
        $user = User::find(Auth::user()->id);
        $deletedUser = UserDump::forcecreate([
            'username' => $user->username,
            'email' => $user->email,
            'role' => $user->role,
            'dob' => $user->dob,
            'favourite_games' => $user->favourite_games,
            'avatar' => $user->avatar,
            'password' => $user->password,
            'user_created_at' => $user->created_at,
            'deleted_at' => Carbon::now()
        ]);
        DB::table('users')->where('id', '=', Auth::user()->id)->delete();
        Auth::logout();
        session()->flash('success', 'Successfully deleted your account. Feel free to join any time');
        return redirect('/login');
    }
}
