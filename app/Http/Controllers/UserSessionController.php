<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserSessionController extends Controller
{
    public function __construct()
	{
		$this->middleware('guest')->except(['destroy']);
    }
    
    public function create()
    {
        return view('user.sessions.create');
    }

    public function store(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $remember = request('remember-me');

        if (Auth::attempt($credentials, $remember)) {
            
            // Authentication passed
            return redirect('/');
        }
    	// if (! auth()->attempt(request(['email', 'password']))) {
    	// 	return back()->withErrors([
        //         'message' => 'Please check your credentials and try again'
        //     ]);
    	// }
        return back()->withErrors([
            'message' => 'Please check your credentials and try again'
        ]);
    }

    public function destroy()
    {
        // auth()->logout();
        Auth::logout();
        session()->flash('success', 'You have been logged out. See you again');
    	return redirect('/login');
    }
}
