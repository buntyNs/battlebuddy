<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function send(Request $request)
    {
        $text = request()->text;
        event(new SendMessage($text));
    }

    
}
