<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Storage;

class UserRegistrationController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function create()
    {
        // $countries = Country::all();
        // $hearAboutUs = HearAboutUs::all()->where('deleted', '=', 0);
        return view('user.register.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'username' => 'bail|required|max:50|min:2',
            'password' => 'required|confirmed|max:15|min:8',
            'region' => 'required',
            'email' => 'required|email',
            'dob' => 'required|date',
            'favourite-games' => 'required|array',
            'terms-and-conditions' => 'required|accepted'
        ]);

        // $hashedPassword = Hash::make(request('password'));
        $hashedPassword = bcrypt(request('password'));
        $favouriteGames = implode(',', request('favourite-games'));
        // $avatar = Storage::disk('public')->put("uploads", url('/public/image/profile/profile-1.jpg'));
        $avatar = 1;

        DB::beginTransaction();
        try {
            $user = User::create([  
                'username' => request('username'), 
                'region' => request('region'),
                'password' => $hashedPassword,
                'email' => request('email'),
                'dob' => request('dob'),
                'favourite_games' => $favouriteGames,
                'dob' => request('dob'),
                'role' => 'user',
                'avatar' => $avatar
            ]);

            DB::commit();
            auth()->login($user);
            // Mail::to($user)->send(new Welcome($user));
            session()->flash('success', 'Thanks so much for signing up!');
            return redirect('/');
        } catch (\PDOException $e) {
            DB::rollback();
            // dd($e);
            if ($e->errorInfo[1] == 1062) {
                $errorCode = $e->errorInfo[1];
                session()->flash('failed-duplicate-email', 'Your entered email is already taken'); 
                return back();   
            }
            session()->flash('failed', 'Something went wrong, please try again');
            return redirect('/register');
        }
    }
}
