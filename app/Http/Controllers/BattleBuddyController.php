<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\BattleBuddy;
use DB;
use App\User;
use Illuminate\Support\Facades\Input;
use App\Events\PostSubmit;
use App\Events\FormSubmitted;

class BattleBuddyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'index', 'indexUsingAJAX', 'searchUsingAJAX']);
    }

    public function index()
    {
        // $games = DB::table('games')->get();
        $popularFeeds = DB::table('feeds')
        ->orderBy('like_count', 'desc')
        ->limit(2)
        ->get();

        return view('user.battle-buddy', compact('popularFeeds'));
    }

    public function indexUsingAJAX(Request $request)
    {
        $latestPosts = BattleBuddy::orderBy('created_at', 'desc')->limit(3)->get();
        $avatars = [];
        foreach ($latestPosts as $latestPost) {
            $userId = $latestPost->user_id;
            $avatarLocation = User::find($userId)->avatar == 1 ? '/img/user/avatar.png' : '/storage/'. User::find($userId)->avatar;
            $avatars[] = $avatarLocation;   
        }

        $arr = array('msg' => 'Something went wrong. Please try again later', 'status' => false);
        if ($latestPosts) {
            $arr = array('msg' => 'Successfully searched', 'status' => true, 'data' => $latestPosts, 'avatars' => $avatars);
        }
        return Response()->json($arr);
    }

    public function store(Request $request)
    {
        $request->validate([
            'game_name' => 'bail|required',
            'platform' => 'bail|required',
            'region' => 'bail|required',
            'language' => 'required',
            'voice_chat' => 'required',
            'gamer_id' => 'required|min:3|max:30',
            'message' => 'required|min:3|max:100'
        ]);

        DB::beginTransaction();
        try {
            $post = BattleBuddy::create([  
                'game_name' => request('game_name'), 
                'platform' => request('platform'),
                'region' => request('region'),
                'language' => request('language'),
                'voice_chat' => request('voice_chat'),
                'gamer_id' => request('gamer_id'),
                'message' => request('message')
            ]);

            DB::commit();
            session()->flash('success', 'Successfully created a post');
            return redirect('/battle-buddy');
        } catch (\PDOException $e) {
            DB::rollback();
            // dd($e);
            // if ($e->errorInfo[1] == 1062) {
            //     $errorCode = $e->errorInfo[1];
            //     session()->flash('failed-duplicate-email', 'Your entered email is already taken');    
            // }
            session()->flash('failed', 'Something went wrong, please try again');
            return redirect('/battle-buddy');
        }
    }

    public function storeUsingAJAX(Request $request)
    {  
        $request->validate([
            'game_name' => 'bail|required',
            'platform' => 'bail|required',
            'region' => 'bail|required',
            'language' => 'required',
            'voice_chat' => 'required',
            'gamer_id' => 'required|min:3|max:30',
            'message' => 'required|min:3|max:100'
        ]);
            
        $post = BattleBuddy::create([  
            'user_id' => Auth::id(),
            'username' => Auth::user()->username,
            'game_name' => request('game_name'),  
            'platform' => request('platform'),
            'region' => request('region'),
            'language' => request('language'),
            'voice_chat' => request('voice_chat'),
            'gamer_id' => request('gamer_id'),
            'message' => request('message'),
            'active' => 1
        ]);
        
        $date = $post->created_at->diffForHumans();
        $avatar = (Auth::user()->avatar == 1) ? '/img/user/avatar.png' : '/storage/' . Auth::user()->avatar;
        $arr = array('msg' => 'Something went wrong. Please try again later', 'status' => false);
        if ($post) { 
            $arr = array('msg' => 'Successfully created the post', 'status' => true, 'data' => $post, 'date' => $date, 'avatar' => $avatar);
            // event(new PostSubmit("post-submit"));
            event(new FormSubmitted('new-post'));
        }
        return Response()->json($arr);
    }

    public function searchUsingAJAX(Request $request)
    {
     
        $post = BattleBuddy::where('active', '=', 1);
       

        if(request('platform') != ""){
            $post = $post->where('platform', '=', request('platform'));
        }

        if(request('region') != ""){
            $post = $post->where('region', '=', request('region'));
        }

        if(request('language') != ""){
            $post = $post->where('language', '=', request('language'));
        }

        if(request('voice_chat') != ""){
            $post = $post->where('voice_chat', '=', request('voice_chat'));
        }

        if(request('game_name') != ""){
            $post = $post->where('game_name', '=', request('game_name'));
        }
        
        $post = $post->orderBy('created_at', 'desc')->get();

        // $post = "Tst";

        // $date = $post->created_at->diffForHumans();
        $arr = array('msg' => 'Something went wrong. Please try again later', 'status' => false);
        if ($post) {
            $arr = array('msg' => 'Successfully searched', 'status' => true, 'data' => $post);
        }
        return Response()->json($arr);
    }
}
