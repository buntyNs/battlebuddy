<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use DB;
use Storage;
use App\Feed;
use App\Comment;
use App\Like;
use App\User;

class FeedsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'index', 'indexUsingAJAX', 'show', 'mostLiked', 'latest']);
    }

    public function index()
    {
        $latestFeeds = DB::table('feeds')
            ->join('users', 'users.id', '=', 'feeds.user_id')
            ->select('feeds.*', 'users.username')
            ->orderBy('created_at', 'desc')
            ->paginate(2);

        // $latestFeeds = DB::table('users')
        //     ->join('feeds', 'feeds.user_id', '=', 'users.id')
        //     ->join('comments', 'comments.user_id', '=', 'users.id')
        //     ->select('feeds.*', 'users.username', 'comments.')
        //     ->latest('feeds.created_at')
        //     ->paginate(2);

        // foreach ($latestFeeds as $latestFeed) {
        //     $feedIds[] = $latestFeed->id;
        //     $feedCount = count($feedIds);

        //     for ($i=0; $i < $feedCount; $i++) {
        //         $feedDetails[] = Comment::all()->where('feed_id', $feedIds[$i]);
        //     }
        // }

        return view('user.feeds.index', compact('latestFeeds'));
    }

    public function mostLiked()
    {
        $mostLikedFeeds = DB::table('feeds')
                                    ->join('users', 'users.id', '=', 'feeds.user_id')
                                    ->select('feeds.*', 'users.username')
                                    ->orderBy('like_count', 'desc')->latest()->paginate(2);
        return view('user.feeds.most-liked', compact('mostLikedFeeds'));
    }

    public function latest()
    {
        $latestFeeds = DB::table('feeds')
                                    ->join('users', 'users.id', '=', 'feeds.user_id')
                                    ->select('feeds.*', 'users.username')
                                    ->latest()->paginate(2);
        return view('user.feeds.latest', compact('latestFeeds'));
    }

    public function indexUsingAJAX(Request $request)
    {
        $latestFeeds = DB::table('feeds')
            ->join('users', 'users.id', '=', 'feeds.user_id')
            ->select('feeds.*', 'users.username')
            ->orderBy('created_at', 'desc')
            ->paginate(2);

        $arr = array('msg' => 'Something went wrong. Please try again later', 'status' => false);
        if ($latestFeeds) {
            $arr = array('msg' => 'Successfully searched', 'status' => true, 'data' => $latestFeeds);
        }
        return Response()->json($arr);
    }

    public function create()
    {
        return view('user.feeds.create');
    }

    public function show($feedId)
    {
        $feed = Feed::join('users', 'users.id', '=', 'feeds.user_id')
                ->join('likes', 'likes.post_comment_id', 'feeds.id')
                ->select('feeds.*', 'users.username', 'likes.status')
                ->where('feeds.id', $feedId)
                ->get();

        if (!count($feed)) {
            $feed = Feed::join('users', 'users.id', '=', 'feeds.user_id')
                ->select('feeds.*', 'users.username')
                ->where('feeds.id', $feedId)
                ->get();
        }
        $tags = $feed[0]->tags;
        $tags = explode(',', $tags);

        $popularFeeds = DB::table('feeds')
        ->orderBy('like_count', 'desc')->take(2)
        ->get();

        $comments = Comment::join('likes', 'likes.post_comment_id', '=', 'comments.id')
                    ->join('users', 'users.id', '=', 'comments.user_id')
                    ->select('comments.*', 'users.username', 'users.id as user_id', 'users.avatar', 'users.role','likes.status')
                    ->where('comments.feed_id', $feedId)
                    ->where('comments.parent_comment_id_reply_id', '=', null)
                    ->get();

        $commentCount = count(Comment::all());
        return view('user.feeds.show', compact('feed', 'tags', 'comments', 'commentCount', 'popularFeeds'));
    }

    public function userFeeds($userId)
    {
        $feeds = Feed::all()->where('user_id', '=', $userId);
        $user = User::find($userId);
        return view('user.feeds.user-specific-feeds', compact('feeds', 'user'));
    }

    public function userGames($userId)
    {
        $games = User::find($userId)->favourite_games;
        $games = explode(',', $games);
        $games = str_replace("-", " ", $games);
        $user = User::find($userId);
        return view('user.feeds.user-specific-games', compact('games', 'user'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'gamer_id' => 'required',
            'tags' => 'required',
            'video_placeholder' => 'sometimes',
            'image_placeholder' => 'sometimes',
            'description' => 'required|min:5',
            'terms_and_conditions' => 'accepted',
            'content' => 'required'
        ]);

        $addToBattlebuddy = $request->has('add_to_battlebuddy') ? 1 : 0;
        $isOwner = $request->has('is_owner') ? 1 : 0;

        $imagePlaceholder = request('image_placeholder');
        $imagePath = null;
        if (isset($imagePlaceholder)) {
            $imagePath = Storage::disk('public')->put("feed_images", request('image_placeholder'));
        }

        DB::beginTransaction();
        try {
            $feed = Feed::forcecreate([
                'user_id' => Auth::user()->id,
                'name' => request('name'),
                'gamer_id' => request('gamer_id'),
                'tags' => request('tags'),
                'image_path' => $imagePath,
                'video_url' => request('video_placeholder'),
                'description' => request('description'),
                'add_to_battlebuddy' => $addToBattlebuddy,
                'is_owner' => $isOwner,
            ]);

            DB::commit();
            session()->flash('success', 'Successfully created a blog item');
            return redirect('/feeds');
        } catch (\PDOException $e) {
            DB::rollback();
            // dd($e);
            // if ($e->errorInfo[1] == 1062) {
            //     $errorCode = $e->errorInfo[1];
            //     session()->flash('failed-duplicate-email', 'Your entered email is already taken');
            // }
            session()->flash('failed', 'Something went wrong, please try again');
            return redirect('/feeds');
        }
    }

    public function likeFeed(Request $request)
    {
        $this->validate(request(), [
            'feed-id' => 'required',
        ]);

        $like = DB::table('likes')->select('id', 'type_id', 'user_id', 'post_comment_id', 'status')
                                ->where('user_id', '=', Auth::user()->id)
                                ->where('type_id', '=', 1)
                                ->where('post_comment_id', '=', request('feed-id'))
                                ->get();

        if (!(count($like) == 0)) {

            if ($like[0]->post_comment_id == request('feed-id') && $like[0]->status == 1) {

                $arr = array('status' => true);
                return Response()->json($arr);
            } else {

                $likeFeed = DB::table('likes')->where('id', $like[0]->id)->update([
                    'status' => 1
                ]);

                $feed = Feed::find(request('feed-id'));
                $feedId = $feed->id;
                $feedLikeCount = ($feed->like_count == null) ? 0 + 1 : $feed->like_count + 1;

                $feedLike = DB::table('feeds')->where('id', '=', $feedId)->update([
                    'like_count' => $feedLikeCount
                ]);

                if ($likeFeed && $feedLike) {
                    $arr = array('status' => true);
                }

                return Response()->json($arr);
            }
        } else {

            $likeFeed = Like::create([
                'type_id' => 1,     // type 1 is feed
                'user_id' => Auth::user()->id,
                'post_comment_id' => request('feed-id'),
                'status' => '1'       // status 1 is line, 0 is neutral(not liked)
            ]);

            $feed = Feed::find(request('feed-id'));
            $feedId = $feed->id;
            $feedLikeCount = ($feed->like_count == null) ? 0 + 1 : $feed->like_count + 1;

            $feedLike = DB::table('feeds')->where('id', '=', $feedId)->update([
                'like_count' => $feedLikeCount
            ]);
            if ($likeFeed && $feedLike) {
                $arr = array('status' => true);
            }
            return Response()->json($arr);
        }
    }

    public function unlikeFeed(Request $request)
    {
        $this->validate(request(), [
            'feed-id' => 'required',
        ]);

        $like = DB::table('likes')->select('id', 'type_id', 'user_id', 'post_comment_id', 'status')
                                ->where('user_id', '=', Auth::user()->id)
                                ->where('type_id', '=', 1)
                                ->where('post_comment_id', '=', request('feed-id'))
                                ->get();

        if (count($like) == 1) {

            if ($like[0]->post_comment_id == request('feed-id') && $like[0]->status == 1) {

                $likeFeed = DB::table('likes')->where('id', $like[0]->id)->update([
                    'status' => '0'
                ]);

                $feed = Feed::find(request('feed-id'));
                $feedId = $feed->id;
                $feedLikeCount = ($feed->like_count == null) ? 0 : $feed->like_count - 1;

                $feedLike = DB::table('feeds')->where('id', '=', $feedId)->update([
                    'like_count' => $feedLikeCount
                ]);

                if ($likeFeed && $feedLike) {
                    $arr = array('status' => true);
                }

                return Response()->json($arr);
            }
        }
    }
}
