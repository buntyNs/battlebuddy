<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feed;
use App\Comment;
use App\Like;
use App\User;
use Auth;
use DB;

class CommentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(Request $request)
    {
        $this->validate(request(), [
            'feed_id' => 'required',
            'message' => 'required|min:2',
        ]);

    	$comment = Comment::create([
            'message' => request('message'),
            'user_id' => Auth::user()->id,
            'feed_id' => request('feed_id'),
        ]);
            
        $likeStatusOfComment = Like::create([
            'type_id' => 2,
            'user_id' => Auth::user()->id,
            'post_comment_id' => $comment->id,
            'status' => '0'       // status 1 is line, 0 is neutral(not liked)
        ]);

        $date = $comment->created_at->diffForHumans();
        $username = ucwords(User::find($comment->user_id)->username);
        $role = User::find($comment->user_id)->role;
        $userId = Auth::user()->id;
        $avatar = (User::find($comment->user_id)->avatar == 1) ? '/img/user/avatar.png' : '/storage/'.User::find($comment->user_id)->avatar;
        $commentId = $comment->id;
        if ($comment && $likeStatusOfComment) { 
            $arr = array('status' => true, 'data' => $comment, 'date' => $date, 'username' => $username, 'avatar' => $avatar, 'commentId' => $commentId, 'role' => $role, 'userId' => $userId);
        }
        return Response()->json($arr);
    }

    public function likeComment(Request $request)
    {
        $this->validate(request(), [
            'comment-id' => 'required',
        ]);
        
        $like = DB::table('likes')->select('id', 'type_id', 'user_id', 'post_comment_id', 'status')
                                ->where('user_id', '=', Auth::user()->id)
                                ->where('type_id', '=', 2)
                                ->where('post_comment_id', '=', request('comment-id'))
                                ->get();
        
        if (!(count($like) == 0)) {

            if ($like[0]->post_comment_id == request('comment-id') && $like[0]->status == 1) {
                $arr = array('status' => true);
                return Response()->json($arr);
            } else {
                $likeComment = DB::table('likes')->where('id', $like[0]->id)->update([
                    'status' => 1
                ]);

                $comment = Comment::find(request('comment-id'));
                $commentId = $comment->id;
                $commentLikeCount = ($comment->like_count == null) ? 0 + 1 : $comment->like_count + 1; 
        
                $commentLike = DB::table('comments')->where('id', '=', $commentId)->update([
                    'like_count' => $commentLikeCount
                ]);

                if ($likeComment && $commentLike) { 
                    $arr = array('status' => true);
                }

                return Response()->json($arr);
            }
        } else {
            $likeComment = Like::create([
                'type_id' => 2,
                'user_id' => Auth::user()->id,
                'post_comment_id' => request('comment-id'),
                'status' => 1       // status 1 is line, 0 is neutral(not liked)
            ]);
    
            $comment = Comment::find(request('comment-id'));
            $commentId = $comment->id;
            $commentLikeCount = ($comment->like_count == null) ? 0 + 1 : $comment->like_count + 1; 
    
            $commentLike = DB::table('comments')->where('id', '=', $commentId)->update([
                'like_count' => $commentLikeCount
            ]);
            if ($likeComment && $commentLike) { 
                $arr = array('status' => true);
            }
            return Response()->json($arr);
        }
    }

    public function unlikeComment(Request $request)
    {
        $this->validate(request(), [
            'comment-id' => 'required',
        ]);

        $like = DB::table('likes')->select('id', 'type_id', 'user_id', 'post_comment_id', 'status')
                                ->where('user_id', '=', Auth::user()->id)
                                ->where('type_id', '=', 2)
                                ->where('post_comment_id', '=', request('comment-id'))
                                ->get();
                                
        if (count($like) == 1) {
            
            if ($like[0]->post_comment_id == request('comment-id') && $like[0]->status == 1) {
                
                $likeComment = DB::table('likes')->where('id', $like[0]->id)->update([
                    'status' => '0'
                ]);

                $comment = Comment::find(request('comment-id'));
                $commentId = $comment->id;
                $commentLikeCount = ($comment->like_count == null) ? 0 : $comment->like_count - 1; 
        
                $commentLike = DB::table('comments')->where('id', '=', $commentId)->update([
                    'like_count' => $commentLikeCount
                ]);

                if ($likeComment && $commentLike) { 
                    $arr = array('status' => true);
                }

                return Response()->json($arr);
            }
        }
    }

    public function markAsSpam()
    {
        $this->validate(request(), [
            'comment-id' => 'required'
        ]);

        $commentId = request('comment-id');

        $spam = Comment::find(request('comment-id'))->is_spam;
        if ($spam == 1) {
            $comment = DB::table('comments')->where('id', request('comment-id'))
                                            ->update(['is_spam' => 0]);
            $isSpam = 0;
        } else {
            $comment = DB::table('comments')->where('id', request('comment-id'))
                                            ->update(['is_spam' => 1]);
            $isSpam = 1;
        }
        
        if ($comment) {
            $response = array("commentId" => $commentId, "isSpam" => $isSpam);
            return Response()->json($response);
        }
    }

    public function filter()
    {
        $this->validate(request(), [
            'filter' => 'required',
            'feed-id' => 'required'
        ]);

        if (request('filter') == "best") {
            $comments = DB::table('comments')
                                ->join('users', 'users.id', 'comments.user_id')
                                ->join('likes', 'likes.post_comment_id', '=', 'comments.id')
                                ->select('comments.*', 'comments.id as comment_id', 'users.*', 'likes.status')
                                ->where('feed_id', request('feed-id'))
                                ->where('parent_comment_id_reply_id', null)
                                ->orderBy('like_count', 'desc')
                                ->get();
        }

        // if (request('filter') == "latest") {
        //     $comments = Comment::join('likes', 'likes.post_comment_id', 'comments.id')
        //                         ->where('feed_id', request('feed-id'))
        //                         ->orderBy('created_at', 'desc')->get();
        // }

        if (request('filter') == "oldest") {
            $comments = DB::table('comments')
                                ->join('users', 'users.id', 'comments.user_id')
                                ->join('likes', 'likes.post_comment_id', '=', 'comments.id')
                                ->select('comments.*', 'comments.id as comment_id', 'users.*', 'likes.status')
                                ->where('feed_id', request('feed-id'))
                                ->where('parent_comment_id_reply_id', null)
                                ->orderBy('like_count', 'asc')
                                ->get();
        }

        if (request('filter') == "random") {
            $comments = DB::table('comments')
                                ->join('users', 'users.id', 'comments.user_id')
                                ->join('likes', 'likes.post_comment_id', '=', 'comments.id')
                                ->select('comments.*', 'comments.id as comment_id', 'users.*', 'likes.status')
                                ->where('feed_id', request('feed-id'))
                                ->where('parent_comment_id_reply_id', null)
                                ->inRandomOrder()
                                ->get();
        }

        if ($comments) {
            // $username = Auth::user()->username;
            // $avatar = (Auth::user()->avatar == 1) ? "/img/user/avatar.png" : '/storage/'.Auth::user()->avatar ;
            $data = array('data' => $comments);
            return Response()->json($data);
        }
    }

    public function destroy()
    {
        $this->validate(request(), [
            'id' => 'required'
        ]);

        $commentsToBeDeleted = Comment::where('parent_comment_id_reply_id', request('id'))->get();
        $deletedCommentIds = [];

        if (count($commentsToBeDeleted) > 0) {
            foreach ($commentsToBeDeleted as $commentToBeDeleted) {
                $commentId = $commentToBeDeleted->id;
                $deletedCommentIds[] = $commentId;
                $deleteComment = Comment::where('id', $commentId)->delete();
            }
            $deleteComment = Comment::where('id', request('id'))->delete();
            $deletedCommentIds[] = request('id');
            $haveReplyStatusUpdate = DB::table('comments')->where('id', request('id'))->update(['have_reply' => 0]);
            $response = ['id' => $deletedCommentIds];
            return Response()->json($response);
        } else {
            $commentId = request('id');
            $deletedCommentIds[] = $commentId;
            $deleteComment = Comment::where('id', request('id'))->delete();
            $haveReplyStatusUpdate = DB::table('comments')->where('id', request('id'))->update(['have_reply' => 0]);
            $response = ['id' => $deletedCommentIds];
            return Response()->json($response);
        }

        $response = "Failed";
        return Response()->json($response);
    }
}
