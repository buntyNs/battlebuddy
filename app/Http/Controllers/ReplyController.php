<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Like;
use App\User;
use Auth;
use DB;

class ReplyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'comment_id' => 'required',
            'feed_id' => 'required',
            'reply' => 'required|min:2',
            'nested' => 'sometimes'
        ]);

    	$reply = Comment::forcecreate([
            'parent_comment_id_reply_id' => request('comment_id'),
            'user_id' => Auth::user()->id,
            'feed_id' => request('feed_id'),
            'message' => request('reply')
        ]);

        if (Comment::find(request('comment_id'))->have_reply == 1) {
            $parentComment = true;
        } else {
            $parentComment = DB::table('comments')->where('id', request('comment_id'))->update(['have_reply' => 1]);
        }

        $likeStatusOfComment = Like::create([
            'type_id' => 2,
            'user_id' => Auth::user()->id,
            'post_comment_id' => $reply->id,
            'status' => '0'       // status 1 is line, 0 is neutral(not liked)
        ]);

        if (request('nested') == '1') {
            $i = '1';
        } else {
            $i = '0';
        }

        $date = $reply->created_at->diffForHumans();
        $commentId = $reply->id;
        $username = ucwords(User::find($reply->user_id)->username);
        $role = User::find($reply->user_id)->role;
        $userId = Auth::user()->id;
        $avatar = (User::find($reply->user_id)->avatar == 1) ? '/img/user/avatar.png' : '/storage/'.User::find($reply->user_id)->avatar;
        if ($reply && $parentComment && $likeStatusOfComment) { 
            $arr = array('status' => true, 'data' => $reply, 'date' => $date, 'username' => $username, 'avatar' => $avatar, 'role' => $role, 'userId' => $userId, 'commentId' => $commentId, 'i' => $i);
        }
        return Response()->json($arr);
    }

    public function load(Request $request)
    {
        $this->validate(request(), [
            'commentId' => 'required'
        ]);

        $replies = DB::table('comments')
                            ->join('users', 'users.id', 'comments.user_id')
                            ->join('likes', 'likes.post_comment_id', '=', 'comments.id')
                            ->select('comments.*', 'comments.id as comment_id', 'users.*', 'likes.status')
                            ->where('parent_comment_id_reply_id', request('commentId'))
                            ->get();

        $userId = Auth::user()->id;

        if ($replies) {
            $data = array('data' => $replies, 'userId' => $userId);
            return response()->json($data, 200);
        }
    }
}
