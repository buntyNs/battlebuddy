<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feed;
use DB;
use Auth;

class HomeController extends Controller
{
    public function index()
    {
        $currentLoggedInUserRegion[0] = (object)['region' => 'europe'];
        $currentLoggedInUserPlatform[0] = (object)['platform' => 'pc'];
        $currentLoggedInUserGames = (array)(object)['0' => 'call-of-duty-ww2', '1' => 'apex-legends'];

        if (Auth::check()) {
            $currentLoggedInUserRegion = DB::table('users')->where('id', Auth::user()->id)->select('region')->get();
            if (count($currentLoggedInUserRegion) == 0) {
                $currentLoggedInUserRegion[0] = (object)['region' => 'europe'];
            }
            $currentLoggedInUserPlatform = DB::table('battle_buddies')->where('user_id', Auth::user()->id)->select('platform')->get();
            if (count($currentLoggedInUserPlatform) == 0) {
                $currentLoggedInUserPlatform[0] = (object)['platform' => 'pc'];
            }
            $currentLoggedInUserGames = DB::table('users')->where('id', Auth::user()->id)->select('favourite_games')->get();
            $currentLoggedInUserGames = explode(',', $currentLoggedInUserGames[0]->favourite_games);
            if (count($currentLoggedInUserGames) == 0) {
                $currentLoggedInUserGames = (array)(object)['0' => 'call-of-duty-ww2', '1' => 'apex-legends'];
            }
        }

        $battleBuddyPC = DB::table('battle_buddies')
        ->where('platform', $currentLoggedInUserPlatform[0]->platform)
        ->orderBy('created_at', 'desc')
        ->limit(1)
        ->get();

        $battleBuddyRegion = DB::table('battle_buddies')
        ->where('region', $currentLoggedInUserRegion[0]->region)
        ->orderBy('created_at', 'desc')
        ->limit(1)
        ->get();
        $battleBuddyGames = DB::table('battle_buddies')
        ->whereIn('user_id', $currentLoggedInUserGames)
        ->orderBy('created_at', 'desc')
        ->limit(1)
        ->get();

        $latestFeeds = DB::table('feeds')
        ->join('users', 'users.id', 'feeds.user_id')
        ->orderBy('feeds.created_at', 'desc')
        ->limit(3)
        ->get();

        $popularFeeds = DB::table('feeds')
        ->orderBy('like_count', 'desc')
            ->take(2)
        ->get();

        return view('user.home', compact('latestFeeds', 'popularFeeds', 'battleBuddyPC', 'battleBuddyRegion', 'battleBuddyGames'));
    }

    public function contact()
    {
        return view('user.contact');
    }
}
