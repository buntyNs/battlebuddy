<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BattleBuddy extends Model
{
    protected $fillable = [
        'user_id',
        'username',
        'game_name',
        'gamer_id',
        'platform',
        'online_time',
        'voice_chat',
        'region',
        'language',
        'message',
        'active'
    ];
}
