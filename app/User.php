<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'region',
        'password',
        'email',
        'dob',
        'favourite_games',
        'role',
        'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function hasRole($role) {
        if ($role) {
            return $this->role == $role;
        }
        return !! $this->role;
    }

    public function feeds()
    {
        return $this->hasMany(Feed::class);
    }

    public function publish(Feed $feed)
    {
        // This can only do because we have a relationship with User - posts() and it automatically adds the user_id in the process
        // If you use create instead of save, you have to pass an array to it.
        
        // $this->posts()->save($post);

        // Create a new post using the request data
        // Save it to the database
        // Post::create([
        //     'title' => request('title'),
        //     'body' => request('body'),
        //     'user_id' => auth()->id()
        // ]);
    }
}
