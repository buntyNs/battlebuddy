<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Feed extends Model
{
    protected $fillable = [
        'name',
        'gamer_id',
        'tags',
        'image_path',
        'video_url',
        'description',
    ]; 

    public function comments()
	{
		return $this->hasMany(Comment::class);
    }
    
    public function user()
	{
		return $this->belongsTo(User::class);
    }
    
    public function addComment($body) {
		// We have a relationship with comments and with eloquent we can do this
		// $this->comments()->create(), behind the scene this will set the id of the post of the comment
		$this->comments()->create(compact('body'));
    }
    
    public function scopeFilter($query, $filters)
	{
		if ($month = $filters['month']) {
		    // whereMonth() is located in Builder class and it expects a number(1-12) as the month
		    $query->whereMonth('created_at', Carbon::parse($month)->month);     // March => 3, May => 5
		}

		if ($year = $filters['year']) {
		    $query->whereYear('created_at', $year);
		}
	}

	public static function archives()
	{
		return static::selectRaw('year(created_at) year, monthname(created_at) month, count(*) published')
            ->groupBy('year', 'month')
            ->orderByRaw('min(created_at) desc')
            ->get()
            ->toArray();
	}

	public function tags()
	{
	    return $this->belongsToMany(Tag::class);
	}
}
