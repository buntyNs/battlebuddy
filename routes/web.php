<?php
// Route::get('/user', function() {
//     $user = App\User::forceCreate([
//         'username' => 'chanaka',
//         'password' => bcrypt('123456789'),
//         'email' => 'chanaka@example.com',
//         'dob' => '1992-11-01',
//         'role' => 'admin'
//     ]);
// });
// Route::get('/type', function() {
//     $user = App\Type::forceCreate([
//         'id' => '2',
//         'name' => 'comment'
//     ]);
// });
use App\Events\FormSubmitted;


Route::get('/login', 'UserSessionController@create')->name('login');
Route::post('/login', 'UserSessionController@store');
Route::get('/logout', 'UserSessionController@destroy');

Route::get('/register', 'UserRegistrationController@create');
Route::post('/register', 'UserRegistrationController@store');

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::post('/', 'HomeController@index');   // Only for the search bar. Temporary. Testing purposes.
Route::get('/contact', 'HomeController@contact');

Route::get('/battle-buddy', 'BattleBuddyController@index');
Route::get('/battle-buddy-get', 'BattleBuddyController@indexUsingAJAX');
Route::get('/battle-buddy/create', 'BattleBuddyController@indexForNotLoggedIn');
// Route::post('/battle-buddy', 'BattleBuddyController@store');
Route::post('/battle-buddy', 'BattleBuddyController@storeUsingAJAX');
Route::post('/battle-buddy/search', 'BattleBuddyController@searchUsingAJAX');



Route::get('/feeds', 'FeedsController@index');
Route::get('/feeds/best-voted', 'FeedsController@mostLiked');
Route::get('/feeds/latest', 'FeedsController@latest');
Route::get('/feeds-get', 'FeedsController@indexUsingAJAX');
Route::get('/feeds/create', 'FeedsController@create');
Route::post('/feeds/create', 'FeedsController@store');
Route::get('/feeds/show/{feedId}', 'FeedsController@show');

// Route::post('/feeds/{feed}/comments', 'CommentsController@store');
Route::post('/feeds/comments/create', 'CommentsController@create');
Route::post('/feeds/comments/reply/store', 'ReplyController@store');
Route::post('/feeds/comments/reply/load', 'ReplyController@load');
Route::get('/feeds/comments/like', 'CommentsController@likeComment');
Route::get('/feeds/comments/unlike', 'CommentsController@unlikeComment');
Route::get('/feeds/like', 'FeedsController@likeFeed');
Route::get('/feeds/unlike', 'FeedsController@unlikeFeed');
Route::get('/feeds/comments/spam', 'CommentsController@markAsSpam');
Route::get('/feeds/comments/filter', 'CommentsController@filter');
Route::get('/feeds/comments/delete', 'CommentsController@destroy');
Route::get('/feeds/user/{userId}', 'FeedsController@userFeeds');
Route::get('/feeds/user/games/{userId}', 'FeedsController@userGames');

// Route::get('/user/profile', 'ProfileController@index');
Route::get('/user/profile/my-feeds', 'ProfileController@allMyFeeds');
Route::get('/user/profile/my-feeds/{userId}', 'ProfileController@allMyFeed');
Route::get('/user/profile/change-email', 'ProfileController@changeEmail');
Route::post('/user/profile/change-email', 'ProfileController@newEmail');
Route::get('/user/profile/change-account-settings', 'ProfileController@changeAccountSettings');
Route::post('/user/profile/change-account-settings', 'ProfileController@storeAccountSettings');
Route::get('/user/profile/delete-my-account', 'ProfileController@deleteAccount');
Route::get('/user/profile/delete-account', 'ProfileController@deleteMyAccount');
Route::get('/user/profile/my-games', 'ProfileController@myGames');
Route::post('/user/profile/my-games', 'ProfileController@storeMyGames');


Route::get('/sender', function () {
    return view('sender');
});

Route::post('/sender', function () {
    
    $text = request()->message;
    $from = request()->from;
    $to = request()->to;
    $name = request()->name;
    $sender = request()->sender;
    $json = "{'from' : '". $from ."' , 'to' : '". $to ."' , 'message' : '". $text ."' , 'name' : '". $name ."' , 'sender' : '". $sender ."'}";
    // dd($json);
    event(new FormSubmitted($json));
});









// Route::get('/', function () {
//     return view('home');
// });

// Route::get('/contact', function () {
//     return view('contact');
// });

// Route::get('/battlebuddy', function () {
//     return view('battlebuddy');
// });

// Route::get('/feeds', function () {
//     return view('feeds');
// });

// Route::get('/forum', function () {
//     return view('forum');
// });

// Route::get('/login', function () {
//     return view('login');
// });

// Route::get('/register', function () {
//     return view('register');
// });

// Route::get('/forum_topic', function () {
//     return view('forum_topic');
// });

// Route::get('/forum_post', function () {
//     return view('forum_post');
// });

// Route::get('/forum_create', function () {
//     return view('forum_create');
// });

// Route::get('/login', function () {
//     return view('login');
// });
